package content;

import engine.Color;
import engine.objects.Player;
import engine.objects.items.EquippableItem;

public class KnightArmorList{
	
	private final EquippableItem[] items = {
		
		new EquippableItem(
			"Iron Armor",
			"A set of basic knights' armor.",
			Player.CLASS_KNIGHT, 1,
			10,
			Color.GRAY,
			new Color[]{
				Color.GRAY,
				Color.RED
			}
		),

		new EquippableItem(
			"Chainmail Armor",
			"A well-crafted, defensive set of chainmail armor.",
			Player.CLASS_KNIGHT, 2,
			15,
			new Color[]{
				Color.GRAY,
				Color.DARK_GRAY
			},
			new Color[]{
				Color.GRAY,
				Color.RED,
				Color.DARK_GRAY
			}
		),
	};
	
	public EquippableItem get(String name){
		
		for(EquippableItem a:items)
			if(a.getName().equalsIgnoreCase(name))
				return a;
		
		return null;
	}
	
	public EquippableItem get(int index){
		return items[index];
	}
}