package content;

import engine.Color;
import engine.objects.items.ConsumableItem;
import engine.objects.items.StatusEffect;
import engine.ui.Icon;

public class ConsumableList{
	
	private final Color[]
		COL_HP	= getBottleColors(Color.RED),
		COL_MP	= getBottleColors(Color.BLUE);
	
	
	private final ConsumableItem[] items = {
		
		// HP potions
		
		new ConsumableItem(
			"Basic HP Potion",
			"A small, magic potion that restores health.",
			1,
			50, 0,
			Icon.ICON_BOTTLE_S,
			COL_HP
		),
		
		new ConsumableItem(
			"HP Potion",
			"A potion that restores your health and spirit.",
			2,
			100, 0,
			Icon.ICON_BOTTLE_M,
			COL_HP
		),
		
		new ConsumableItem(
			"MP Buff Potion",
			"A potion that blah blah",
			200,
			new StatusEffect[]{new StatusEffect(StatusEffect.TYPE_MP_BUFF, 50, 1, 100, true)},
			Icon.ICON_BOTTLE_XL,
			COL_MP
		),
		
		
		
		
		// MP Potions
		
		new ConsumableItem(
			"Basic MP Potion",
			"A small, magic potion that restores mana.",
			1,
			0, 50,
			Icon.ICON_BOTTLE_S,
			COL_MP
		),
		
		new ConsumableItem(
			"MP Potion",
			"A potion that restores your mana and energy.",
			2,
			0, 100,
			Icon.ICON_BOTTLE_M,
			COL_MP
		),
	};
	
	private Color[] getBottleColors(Color color){
		return new Color[]{
			color,
			Color.BROWN,
			Color.WHITE
		};
	}
	
	public ConsumableItem get(String name){
		
		for(ConsumableItem a:items)
			if(a.getName().equalsIgnoreCase(name))
				return a;
		
		return null;
	}
	
	public ConsumableItem get(int index){
		return items[index];
	}
}
