package content;

import engine.Color;
import engine.objects.Player;
import engine.objects.items.Attack;
import engine.objects.items.Weapon;

public class KnightWeaponList{
	
	private final Weapon[] items = {
		
		new Weapon(
			"Iron Sword",
			"A crude iron sword.",
			Player.CLASS_KNIGHT, 1, false,
			new Color[]{
				Color.GRAY
			},
			new Attack[]{
				new Attack(
					"Basic Slash",
					"A basic, weak slash that hits often.",
					10, 15, 75
				),
				new Attack(
					"Strong Slash",
					"Stronger than a basic slash, but harder to hit.",
					20, 35, 50
				),
			}
		),
	};
	
	public Weapon get(String name){
		
		for(Weapon a:items)
			if(a.getName().equalsIgnoreCase(name))
				return a;
		
		return null;
	}
	
	public Weapon get(int index){
		return items[index];
	}
}