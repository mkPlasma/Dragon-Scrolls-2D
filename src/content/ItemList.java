package content;

import engine.objects.Player;
import engine.objects.items.ConsumableItem;
import engine.objects.items.EquippableItem;
import engine.objects.items.Weapon;

public class ItemList{
	
	private final KnightWeaponList	knightWeaponList;
	private final KnightArmorList	knightArmorList;
	
	private final ConsumableList	consumableList;
	
	public ItemList(){
		knightWeaponList	= new KnightWeaponList();
		knightArmorList		= new KnightArmorList();
		
		consumableList		= new ConsumableList();
	}
	
	
	
	public Weapon getWeapon(String name){
		
		Weapon w = knightWeaponList.get(name);
		
		if(w != null)
			return w;
		
		return null;
	}
	
	public Weapon getWeapon(int classType, int index){
		switch(classType){
			case Player.CLASS_KNIGHT:	return knightWeaponList.get(index);
		}
		
		return null;
	}
	
	
	
	public EquippableItem getArmor(String name){
		
		EquippableItem a = knightArmorList.get(name);
		
		if(a != null)
			return a;
		
		return null;
	}
	
	public EquippableItem getArmor(int classType, int index){
		switch(classType){
			case Player.CLASS_KNIGHT:	return knightArmorList.get(index);
		}
		
		return null;
	}

	
	public ConsumableItem getConsumable(String name){
		return consumableList.get(name);
	}
	
	public ConsumableItem getConsumable(int index){
		return consumableList.get(index);
	}
}
