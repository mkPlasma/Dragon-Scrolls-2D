package mapedit;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import engine.objects.Map;
import engine.objects.Tile;

public class MEPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	// Camera
	private int cx, cy;
	private float zoom = 1;
	
	// Tile position
	private int tx, ty, tz;
	// Dragging
	private int tx2, ty2;
	
	// Mouse
	private int mx, my, mdx, mdy, mpx, mpy;
	private boolean mouseDown, rightMouseDown;
	private boolean rightMouseDownFirst;
	private boolean mouseReleased, rightMouseReleased;
	private boolean panning;
	
	private boolean replaceMode;
	
	private METile[] tileset;
	private int tile;
	
	private Map map;
	private Tile[][][] tiles;
	private MERenderer renderer;
	
	public MEPanel(METile[] tileset){
		this.tileset = tileset;
		
		tiles = new Tile[30][30][10];
		map = new Map(tiles);
		renderer = new MERenderer(map);
		
		int s = tiles.length/2;
		cx = 320;
		cy = 240 - s*16;
	}
	
	public void paint(Graphics g){
		renderer.update(cx, cy, zoom, tx, ty, tz, tx2, ty2, rightMouseDown, (Graphics2D)g);
		renderer.render();
	}
	
	public void update(){
		
		// Add tiles
		if(mouseReleased || rightMouseReleased){
			
			int sx = Math.min(tx, tx2);
			int sy = Math.min(ty, ty2);
			int ex = Math.max(tx, tx2);
			int ey = Math.max(ty, ty2);
			
			for(int x = sx; x <= ex; x++){
				for(int y = sy; y <= ey; y++){
					map.setTile(x, y, tz, rightMouseReleased ? null : new METile(tileset[tile]));

					for(int x2 = x - 1; x2 <= x + 1; x2++)
						for(int y2 = y - 1; y2 <= y + 1; y2++)
							for(int z2 = 0; z2 <= tz + 1; z2++)
									updateTileShadows(x2, y2, z2);
				}
			}
			
			mouseReleased = false;
			rightMouseReleased = false;
			
			updateTilePos();
			updateTPos2();
		}
		
		repaint();
	}
	
	private void updateTileShadows(int x, int y, int z){
		
		if(x < 0 || x >= tiles.length || y < 0 || y >= tiles[x].length || z < 0 || z >= tiles[x][y].length - 1)
			return;
		
		if(tiles[x][y][z] == null)
			return;
		
		Tile t = tiles[x][y][z];
		t.setShadow((short)0);
		
		boolean xMin = x > 0;
		boolean xMax = x < tiles.length - 1;
		boolean yMin = y > 0;
		boolean yMax = y < tiles[x].length - 1;
		boolean zMin = z > 0;
		boolean zMax = z < tiles[x][y].length - 1;
		
		if(zMax && tiles[x][y][z + 1] == null){
			// Regular
			if(xMin && tiles[x - 1][y][z + 1] != null)
				t.addShadow(Tile.SHADOW_LEFT);
			
			if(xMax && tiles[x + 1][y][z + 1] != null)
				t.addShadow(Tile.SHADOW_RIGHT);
			
			if(yMin && tiles[x][y - 1][z + 1] != null)
				t.addShadow(Tile.SHADOW_TOP);
			
			if(yMax && tiles[x][y + 1][z + 1] != null)
				t.addShadow(Tile.SHADOW_BOTTOM);
			
			// Corners
			if(xMin && yMin && tiles[x - 1][y - 1][z + 1] != null && !t.hasShadow(Tile.SHADOW_LEFT) && !t.hasShadow(Tile.SHADOW_TOP))
				t.addShadow(Tile.SHADOW_CORNER_TOP);
			
			if(xMin && yMax && tiles[x - 1][y + 1][z + 1] != null && !t.hasShadow(Tile.SHADOW_LEFT) && !t.hasShadow(Tile.SHADOW_BOTTOM))
				t.addShadow(Tile.SHADOW_CORNER_LEFT);
			
			if(xMax && yMin && tiles[x + 1][y - 1][z + 1] != null && !t.hasShadow(Tile.SHADOW_RIGHT) && !t.hasShadow(Tile.SHADOW_TOP))
				t.addShadow(Tile.SHADOW_CORNER_RIGHT);
			
			//if(xMax && yMax && tiles[x + 1][y + 1][z + 1] != null && !t.hasShadow(Tile.SHADOW_RIGHT) && !t.hasShadow(Tile.SHADOW_BOTTOM))
			//	t.addShadow(Tile.SHADOW_CORNER_BOTTOM);
			
			// Full
			boolean[] full = new boolean[9];
			boolean dark = true;
			
			for(int z2 = z + 2; z2 < tiles[x][y].length - 1; z2++){
				
				if(tiles[x][y][z2] != null)
					full[0] = true;
				
				if(xMin && tiles[x - 1][y][z2] != null)
					full[1] = true;
				
				if(xMax && tiles[x + 1][y][z2] != null)
					full[2] = true;
				
				if(yMin && tiles[x][y - 1][z2] != null)
					full[3] = true;
				
				if(yMax && tiles[x][y + 1][z2] != null)
					full[4] = true;
				
				if(xMin && yMin && tiles[x - 1][y - 1][z2] != null)
					full[5] = true;
				
				if(xMin && yMax && tiles[x - 1][y + 1][z2] != null)
					full[6] = true;
				
				if(xMax && yMin && tiles[x + 1][y - 1][z2] != null)
					full[7] = true;
				
				if(xMax && yMax && tiles[x + 1][y + 1][z2] != null)
					full[8] = true;
				
				
				dark = true;
				
				for(boolean b:full)
					if(!b)
						dark = false;
				
				if(dark){
					t.addShadow(Tile.SHADOW_FULL_DARK);
					break;
				}
			}
			
			if(full[0] && !dark)
				t.addShadow(Tile.SHADOW_FULL_LIGHT);
		}

		// Sides
		if(zMin){
			if(yMax && tiles[x][y + 1][z - 1] != null && tiles[x][y + 1][z] == null)
				t.addShadow(Tile.SHADOW_SIDE_LEFT);
			
			if(xMax && tiles[x + 1][y][z - 1] != null && tiles[x + 1][y][z] == null)
				t.addShadow(Tile.SHADOW_SIDE_RIGHT);
		}
	}
	
	public void setTile(int tile){
		this.tile = tile;
	}
	
	public void setMousePos(int mx, int my){
		
		if(panning){
			cx -= (mpx - mx);
			cy -= (mpy - my);
			mpx = mx;
			mpy = my;
			
			checkCameraPos();
			return;
		}
		
		mpx = mx;
		mpy = my;
		
		mx -= cx;
		my -= cy;
		
		my -= 25;
		
		my += 8*zoom;
		my *= 2;
		
		if(!mouseDown && !rightMouseDown){
			this.mx = mx;
			this.my = my;
		}
		else{
			mdx = mx;
			mdy = my;
		}
		
		updateTilePos();
	}
	
	public void updateTilePos(){
		updateTPos();
		updateTPos2();
	}
	
	private void updateTPos(){
		
		if(mouseReleased || rightMouseReleased)
			return;
		
		if(!rightMouseDownFirst && (mouseDown || rightMouseDown)){
			int mx = mdx - this.mx;
			int my = mdy - this.my;
			
			tx2 = tx + (int)Math.round((mx + my)/(32*zoom));
			ty2 = ty - (int)Math.round((mx - my)/(32*zoom));
			
			if(tx2 < 0)
				tx2 = 0;
			
			if(tx2 >= tiles.length)
				tx2 = tiles.length - 1;
			
			if(ty2 < 0)
				ty2 = 0;
			
			if(ty2 >= tiles.length)
				ty2 = tiles.length - 1;
			
			return;
		}
		
		tx = (int)Math.round((mx + my)/(32*zoom)) - 1;
		ty = -(int)Math.round((mx - my)/(32*zoom)) - 1;
		tz = 0;
		
		// Check boundaries
		if(tx < 0)
			tx = 0;
		
		if(tx >= tiles.length)
			tx = tiles.length - 1;
		
		if(ty < 0)
			ty = 0;
		
		if(ty >= tiles.length)
			ty = tiles.length - 1;
		
		// Check tiles in front
		for(int x = tx, y = ty, z = 0; x < tiles.length && y < tiles[x].length && z < tiles[x][y].length; x++, y++, z++){
			if(tiles[x][y][z] != null || (z > 0 && tiles[x][y][z - 1] != null)){
				tx = x;
				ty = y;
				tz = z;
				
				if(replaceMode && tz > 0)
					tz--;
			}
		}
		
		if(replaceMode || rightMouseDown)
			return;
		
		boolean b = (tx + ty) % 2 == 0;
		
		boolean left = (b && ((mx < 0 && mx % (32*zoom) > -127) || (mx > 0 && mx % (32*zoom) > 127))) || (!b && ((mx < 0 && mx % (32*zoom) < -127) || (mx > 0 && mx % (32*zoom) < 127)));
		
		// Check side tiles
		if(left){
			if(ty < tiles[tx].length - 1 && tiles[tx][ty][tz] != null && tiles[tx][ty + 1][tz] == null){
				ty++;
				return;
			}
			
			for(int x = tx, y = ty, z = 0; x < tiles.length - 1 && y < tiles[x].length - 1 && z < tiles[x][y].length; x++, y++, z++){
				if(tiles[x][y + 1][z] != null){
					tx = x + 1;
					ty = y + 1;
					tz = z;
					return;
				}
			}
		}
		else{
			if(tx < tiles[tx].length - 1 && tiles[tx][ty][tz] != null && tiles[tx + 1][ty][tz] == null){
				tx++;
				return;
			}
			
			for(int x = tx, y = ty, z = 0; x < tiles.length - 1 && y < tiles[x].length - 1 && z < tiles[x][y].length; x++, y++, z++){
				if(tiles[x + 1][y][z] != null){
					System.out.println("a");
					tx = x + 1;
					ty = y + 1;
					tz = z;
					return;
				}
			}
		}
		
		// Check tiles in back
		if(tz > 0 && tiles[tx][ty][tz] == null && ((b && my % (32*zoom) > 16*zoom) || (!b && my % (32*zoom) < 16*zoom))){
			for(int x = tx - 1, y = ty - 1, z = tz - 1; x >= 0 && y >= 0 && z >= 0; x--, y--, z--){
				if(tiles[x][y][z] == null && (z == 0 || (z > 0 && tiles[x][y][z - 1] != null))){
					tx = x;
					ty = y;
					tz = z;
					return;
				}
			}
		}
	}
	
	private void updateTPos2(){
		if(!mouseDown && !rightMouseDown && !mouseReleased && !rightMouseReleased){
			tx2 = tx;
			ty2 = ty;
		}
	}
	
	private void checkCameraPos(){
		int n = (int)((tiles.length - 2)*16*zoom);
		
		if(cx < -n)					cx = -n;
		else if(cx > 640 + n)		cx = 640 + n;
		if(cy < -n)					cy = -n;
		else if(cy > 480 - 16*zoom)	cy = 480 - (int)(16*zoom);
	}
	
	public void setMouseDown(boolean mouseDown){
		
		if(this.mouseDown && !mouseDown)
			mouseReleased = true;
		
		this.mouseDown = mouseDown;
	}
	
	public void setRightMouseDown(boolean rightMouseDown){
		
		if(this.rightMouseDown && !rightMouseDown)
			rightMouseReleased = true;
		
		this.rightMouseDown = rightMouseDown;
	}
	
	public void setPanning(boolean panning){
		this.panning = panning;
	}
	
	public void zoomIn(){
		
		if(panning || zoom >= 8)
			return;
		
		zoom *= 2;

		// Adjust position to zoom in on center rather than (cx, cy)
		cx -= 320 - cx;
		cy -= 240 - cy;
		checkCameraPos();
	}
	
	public void zoomOut(){
		
		if(panning || zoom <= 0.25)
			return;
		
		zoom /= 2;
		
		cx = (320 + cx)/2;
		cy = (240 + cy)/2;
		checkCameraPos();
	}
	
	public void setReplaceMode(boolean replaceMode){
		this.replaceMode = replaceMode;
	}
}
