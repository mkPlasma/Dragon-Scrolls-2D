package mapedit;

import java.awt.image.BufferedImage;

import engine.objects.Tile;

public class METile extends Tile{
	
	private final BufferedImage img;
	
	public METile(METile tile){
		super(tile.getType(), null);
		img = tile.getImage();
	}
	
	public METile(int type, BufferedImage spriteSheet){
		super(type, null);
		img = spriteSheet.getSubimage(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
	}
	
	public BufferedImage getImage(){
		return img;
	}
}
