package mapedit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import engine.objects.Map;
import engine.objects.Tile;

public class MERenderer{
	
	private int cx, cy;
	private float zoom;
	
	private int tx, ty, tz;
	private int tx2, ty2;
	
	private boolean rightMouseDown;
	
	private BufferedImage[] tileShadow;
	
	private Map map;
	private Tile[][][] tiles;
	
	private Graphics2D g2d;
	
	public MERenderer(Map map){
		this.map = map;
		tiles = map.getTiles();
		
		
		BufferedImage shadow = null;
		
		try{
			shadow = ImageIO.read(new File("res/img/tiles/shadow.png"));
		}catch(Exception e){e.printStackTrace();}
		
		
		tileShadow = new BufferedImage[12];
		
		for(int i = 0; i < 12; i++)
			tileShadow[i] = shadow.getSubimage((i % 6)*32, 64 + (i/6)*32, 32, 32);
	}
	
	public void update(int cx, int cy, float zoom, int tx, int ty, int tz, int tx2, int ty2, boolean rightMouseDown, Graphics2D g2d){
		this.cx = cx;
		this.cy = cy;
		this.zoom = zoom;
		this.tx = tx;
		this.ty = ty;
		this.tz = tz;
		this.tx2 = tx2;
		this.ty2 = ty2;
		this.g2d = g2d;
		this.rightMouseDown = rightMouseDown;
	}
	
	public void render(){
		
		// Background
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, 640, 480);
		
		drawGrid();
		drawTiles();
		
		// Arrow to center
		int n = (int)((tiles.length - 2)*16*zoom);
		
		if(cx == -n || cx == 640 + n || cy == -n || cy == 480 - 16*zoom){
			g2d.setColor(Color.BLUE);
			g2d.drawLine(320, 240, cx, cy);
		}
		
		g2d.setColor(Color.WHITE);
		g2d.drawString("x: " + tx2, 4, 438);
		g2d.drawString("y: " + ty2, 4, 454);
		g2d.drawString("z: " + tz, 4, 470);
	}
	
	private void drawGrid(){
		g2d.setColor(Color.DARK_GRAY);
		
		int s = (int)(8*zoom);
		int t = map.getTiles().length;
		
		for(int i = 0; i <= t; i++){
			
			int n = i*s;
			int fs = t*s;
			
			g2d.drawLine(
				cx + n*2,
				cy + n,
				cx + n*2 - fs*2 - 1,
				cy + n + fs
			);
			
			g2d.drawLine(
				cx - n*2 - 1,
				cy + n,
				cx - n*2 + fs*2,
				cy + n + fs
			);
		}
	}
	
	private void drawTiles(){
		
		g2d.scale(zoom, zoom);
		
		for(int x = 0; x < tiles.length; x++){
			for(int y = 0; y < tiles[x].length; y++){
				for(int z = 0; z < tiles[x][y].length; z++){
					METile t = (METile)tiles[x][y][z];
					
					if(t != null){
						
						float sx = cx/zoom + (x - y)*16 - 16;
						float sy = cy/zoom + (x + y - z*2)*8 - 16;
						
						g2d.translate(sx, sy);
						g2d.drawImage(t.getImage(), 0, 0, null);
						
						drawTileShadow(t, sx, sy);
						
						g2d.translate(-sx, -sy);
					}
					
					if(x == Math.max(tx, tx2) && y == Math.max(ty, ty2) && z == tz){
						g2d.scale(1/zoom, 1/zoom);
						drawTileOutline();
						g2d.scale(zoom, zoom);
					}
				}
			}
		}
		
		g2d.scale(1/zoom, 1/zoom);
	}
	
	private void drawTileShadow(Tile t, float sx, float sy){

		boolean ls = t.hasShadow(Tile.SHADOW_LEFT);
		boolean ts = t.hasShadow(Tile.SHADOW_TOP);
		boolean rs = t.hasShadow(Tile.SHADOW_RIGHT);
		boolean bs = t.hasShadow(Tile.SHADOW_BOTTOM);

		boolean lt = ls && ts;
		boolean lb = ls && bs;
		boolean rt = rs && ts;
		boolean rb = rs && bs;
		
		AffineTransform trns = g2d.getTransform();
		AffineTransform flip = new AffineTransform(trns);
		flip.translate(32, 0);
		flip.scale(-1, 1);
		
		// Left/top
		if(!lt && !lb && ls)
			g2d.drawImage(tileShadow[0], 0, 0, null);
		if(!lt && !rt && ts){
			g2d.setTransform(flip);
			g2d.drawImage(tileShadow[0], 0, 0, null);
			g2d.setTransform(trns);
		}
		
		// Right/bottom
		if(!rt && !rb && rs)
			g2d.drawImage(tileShadow[1], 0, 0, null);
		if(!lb && !rb && bs){
			g2d.setTransform(flip);
			g2d.drawImage(tileShadow[1], 0, 0, null);
			g2d.setTransform(trns);
		}
		
		
		// Side
		if(t.hasShadow(Tile.SHADOW_SIDE_LEFT))
			g2d.drawImage(tileShadow[2], 0, 0, null);
		if(t.hasShadow(Tile.SHADOW_SIDE_RIGHT)){
			g2d.setTransform(flip);
			g2d.drawImage(tileShadow[2], 0, 0, null);
			g2d.setTransform(trns);
		}
		
		
		// Full
		if(t.hasShadow(Tile.SHADOW_FULL_LIGHT))
			g2d.drawImage(tileShadow[4], 0, 0, null);
		if(t.hasShadow(Tile.SHADOW_FULL_DARK))
			g2d.drawImage(tileShadow[5], 0, 0, null);
		
		
		// Corners
		if(t.hasShadow(Tile.SHADOW_CORNER_TOP))
			g2d.drawImage(tileShadow[6], 0, 0, null);

		if(t.hasShadow(Tile.SHADOW_CORNER_LEFT))
			g2d.drawImage(tileShadow[7], 0, 0, null);
		if(t.hasShadow(Tile.SHADOW_CORNER_RIGHT)){
			g2d.setTransform(flip);
			g2d.drawImage(tileShadow[7], 0, 0, null);
			g2d.setTransform(trns);
		}
		
		// Two sides
		if(lt)
			g2d.drawImage(tileShadow[8], 0, 0, null);
		if(lb || rt)
			g2d.drawImage(tileShadow[9], 0, 0, null);
		if(rt){
			if(rt) g2d.setTransform(flip);
			g2d.drawImage(tileShadow[9], 0, 0, null);
			if(rt) g2d.setTransform(trns);
		}
	}
	
	private void drawTileOutline(){
		g2d.setStroke(new BasicStroke(3));
		g2d.setColor(rightMouseDown ? Color.RED : Color.GREEN);
		
		int x = cx + (int)(((tx - ty)*16)*zoom) - 1;
		int y = cy + (int)(((tx + ty)*8 - tz*16 - 16)*zoom);
		
		int sx = (tx2 - tx + 1)*16;
		int sy = (ty2 - ty + 1)*16;
		int sz = 16;
		
		if(sx == 0) sx -= 16;
		if(sy == 0) sy -= 16;
		
		sx *= zoom;
		sy *= zoom;
		sz *= zoom;
		
		boolean xn = sx < 0;
		boolean yn = sy < 0;
		
		if(xn){
			sx -= 16*zoom;
			x += 16*zoom;
			y += 8*zoom;
		}
		if(yn){
			sy -= 16*zoom;
			x -= 16*zoom;
			y += 8*zoom;
		}
		
		// Top
		g2d.drawLine(
			x + (xn ? sx : 0) + (yn ? -sy : 0),
			y + (xn ? sx/2 : 0) + (yn ? sy/2 : 0),
			x + (xn ? sx : 0) + (yn ? -sy : 0) + 1,
			y + (xn ? sx/2 : 0) + (yn ? sy/2 : 0)
		);
		
		// Top right, left
		g2d.drawLine(
			x + (xn ? -1 : 2),
			y + (xn ? -1 : 1),
			x + (xn ? 2 : -1) + sx,
			y + (xn ? 1 : -1) + sx/2
		);
		g2d.drawLine(
			x + (yn ? 2 : -1),
			y + (yn ? -1 : 1),
			x + (yn ? -1 : 2) - sy,
			y + (yn ? 1 : -1) + sy/2
		);

		// Middle
		g2d.drawLine(
			x + (xn ? 2 : 0) + (yn ? -1 : 0) + sx - sy,
			y + (xn ? -1 : 0) + (yn ? 1 : 0) + (sx + sy)/2,
			x + (yn ? 3 : 0) + sx - 1,
			y + (yn ? -2 : 0) + sx/2 + 1
		);
		g2d.drawLine(
			x + (xn ? 2 : 1) + (yn ? -2 : 0) + sx - sy,
			y + (xn ? 1 : 0) + (yn ? -1 : 0) + (sx + sy)/2,
			x + (xn ? -1 : 2) - sy,
			y + (xn ? -1 : 1) + sy/2
		);
		
		// Bottom
		g2d.drawLine(
			x + (xn ? 0 : sx) + (yn ? -1 : 0) - sy,
			y + (xn ? 0 : sx/2) + (yn ? 1 : 0) + sy/2 + sz - 1,
			x + (xn ? 0 : sx) + (yn ? 1 : 0) - 1,
			y + (xn ? 0 : sx/2) + (yn ? -1 : 0) + sz
		);
		g2d.drawLine(
			x + (yn ? 0 : -sy) + (xn ? 1 : 0) + sx + 1,
			y + (yn ? 0 : sy/2) + (xn ? 1 : 0) + sx/2 + sz - 1,
			x + (yn ? 0 : -sy) + (xn ? -1 : 0) + 2,
			y + (yn ? 0 : sy/2) + (xn ? -1 : 0) + sz
		);
		
		// Sides
		g2d.drawLine(
			x + (xn ? 0 : sx),
			y + (xn ? 0 : sx/2),
			x + (xn ? 0 : sx),
			y + (xn ? 0 : sx/2) + sz - 1
		);
		g2d.drawLine(
			x + (yn ? 0 : -sy) + 1,
			y + (yn ? 0 : sy/2),
			x + (yn ? 0 : -sy) + 1,
			y + (yn ? 0 : sy/2) + sz - 1
		);
		g2d.drawLine(
			x + (xn ? -1 : sx) + (yn ? 0 : 1) + (xn && yn ? 1 : 0) - sy,
			y + (xn ? 0 : sx/2) + sy/2,
			x + (xn ? -1 : sx) + (yn ? 0 : 1) + (xn && yn ? 1 : 0) - sy,
			y + (xn ? 0 : sx/2) + sy/2 + sz - 1
		);
		g2d.drawLine(
			x + (yn ? 1 : -sy) + (xn ? 1 : 0) + (xn && yn ? -1 : 0) + sx,
			y + (yn ? 0 : sy/2) + sx/2,
			x + (yn ? 1 : -sy) + (xn ? 1 : 0) + (xn && yn ? -1 : 0) + sx,
			y + (yn ? 0 : sy/2) + sx/2 + sz - 1
		);
		
		g2d.setStroke(new BasicStroke());
	}
}
