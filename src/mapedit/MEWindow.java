package mapedit;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.metal.MetalBorders;

import engine.objects.Tile;

public class MEWindow extends JFrame implements ActionListener, MouseListener, MouseMotionListener, MouseWheelListener, KeyListener{
	
	private static final long serialVersionUID = 1L;
	
	private METile[] tileset;
	
	private JCheckBox replaceCheckBox;
	
	private final MEPanel panel;
	private boolean mouseInPanel;
	
	private boolean ctrlDown;
	
	public MEWindow(){
		super("Map Editor");
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1000, 800);
		setResizable(false);
		setLocationRelativeTo(null);
		setFocusable(true);
		
		setLayout(new FlowLayout());

		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		addKeyListener(this);
		
		JScrollPane leftPane = new JScrollPane();
		leftPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		leftPane.setPreferredSize(new Dimension(130, 480));
		
		JScrollPane rightPane = new JScrollPane(genTiles());
		rightPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		rightPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		rightPane.setPreferredSize(new Dimension(200, 480));

		JPanel bottomLeftPanel = new JPanel();
		bottomLeftPanel.setPreferredSize(new Dimension(487, 275));
		bottomLeftPanel.setBorder(MetalBorders.getTextBorder());
		setUpBottomLeftPanel(bottomLeftPanel);
		
		JPanel bottomRightPanel = new JPanel();
		bottomRightPanel.setPreferredSize(new Dimension(488, 275));
		bottomRightPanel.setBorder(MetalBorders.getTextBorder());
		
		panel = new MEPanel(tileset);
		panel.setPreferredSize(new Dimension(640, 480));
		panel.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		
		
		add(leftPane);
		add(panel);
		add(rightPane);
		add(bottomLeftPanel);
		add(bottomRightPanel);
		
		
		setVisible(true);
	}
	
	private JPanel genTiles(){
		
		BufferedImage t01 = null;
		
		try{
			t01 = ImageIO.read(new File("res/img/tiles/01.png"));
		}catch(Exception e){e.printStackTrace();}
		
		
		
		tileset = new METile[Tile.NUM_TILES];
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(100, (Tile.NUM_TILES/2)*40));
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		for(int i = 0; i < Tile.NUM_TILES; i++){
			tileset[i] = new METile(i, t01);
			
			// Tile button
			JButton tb = new JButton(new ImageIcon(tileset[i].getImage()));
			tb.setActionCommand("tile" + i);
			tb.addActionListener(this);
			tb.setSize(new Dimension(40, 40));
			panel.add(tb);
		}
		
		return panel;
	}
	
	private void setUpBottomLeftPanel(JPanel panel){
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		
		panel.add(new JLabel("Options"));
		
		replaceCheckBox = new JCheckBox();
		replaceCheckBox.addActionListener(this);
		replaceCheckBox.setActionCommand("replaceMode");
		replaceCheckBox.setText("Replace mode");
		replaceCheckBox.setMnemonic('r');
		
		panel.add(replaceCheckBox);
	}
	
	public void actionPerformed(ActionEvent e){
		
		String command = e.getActionCommand();
		
		if(command.startsWith("tile")){
			panel.setTile(Integer.parseInt(e.getActionCommand().substring(4)));
			return;
		}
		
		switch(command){
			case "replaceMode":
				setReplaceMode();
				break;
		}
	}
	
	private void setReplaceMode(){
		panel.setReplaceMode(replaceCheckBox.isSelected());
	}
	
	
	public void mouseClicked(MouseEvent e){
		
	}
	
	public void mouseEntered(MouseEvent e){
		
	}
	
	public void mouseExited(MouseEvent e){
		
	}
	
	public void mousePressed(MouseEvent e){
		updatePanelMouseDown(e, true);
		updatePanelMousePos(e);
	}
	
	public void mouseReleased(MouseEvent e){
		updatePanelMouseDown(e, false);
		updatePanelMousePos(e);
	}
	
	public void mouseDragged(MouseEvent e){
		updatePanelMousePos(e);
	}
	
	public void mouseMoved(MouseEvent e){
		mouseInPanel = panel.getBounds().contains(e.getPoint());
		updatePanelMousePos(e);
	}
	
	public void mouseWheelMoved(MouseWheelEvent e){
		
		if(!mouseInPanel)
			return;
		
		if(e.getUnitsToScroll() > 0)
			panel.zoomOut();
		else
			panel.zoomIn();
		
		updatePanelMousePos(e);
	}
	
	
	private void updatePanelMouseDown(MouseEvent e, boolean down){
		
		if(!mouseInPanel)
			return;
		
		switch(e.getButton()){
			case 1: // Left
				panel.setMouseDown(down);
				break;
			case 2: // Middle
				panel.setPanning(down);
				break;
			case 3: // Right
				panel.setRightMouseDown(down);
				break;
		}
	}
	
	private void updatePanelMousePos(MouseEvent e){
		
		if(!mouseInPanel)
			return;
		
		panel.setMousePos(e.getX() - panel.getX(), e.getY() - panel.getY());
		panel.update();
	}
	
	
	
	public void keyTyped(KeyEvent e){
		
	}
	
	public void keyPressed(KeyEvent e){
		switch(e.getKeyCode()){
			
			case KeyEvent.VK_R:
				replaceCheckBox.doClick();
				setReplaceMode();
				panel.updateTilePos();
				panel.update();
				break;
				
			case KeyEvent.VK_CONTROL:
				if(!ctrlDown){
					replaceCheckBox.setSelected(true);
					setReplaceMode();
					
					panel.updateTilePos();
					panel.update();
					
					ctrlDown = true;
				}
				
				break;
		}
	}
	
	public void keyReleased(KeyEvent e){
		switch(e.getKeyCode()){
			
			case KeyEvent.VK_CONTROL:
				if(ctrlDown){
					replaceCheckBox.setSelected(false);
					setReplaceMode();
					
					panel.updateTilePos();
					panel.update();
					
					ctrlDown = false;
				}
				break;
		}
	}
}
