package engine.screens;

import engine.graphics.Renderer;
import engine.graphics.TextureCache;

/*
 * 		GameScreen.java
 * 		
 * 		Purpose:	Game screen.
 * 		Notes:		Simple container for screen-specific
 * 					update and drawing methods.
 * 		
 * 		Children:	MainScreen.java, MapScreen.java
 * 		
 */

public abstract class GameScreen{
	
	protected final ScreenManager manager;
	protected final TextureCache tc;
	protected final Renderer r;
	
	public GameScreen(ScreenManager manager, Renderer r, TextureCache tc){
		this.manager = manager;
		this.r = r;
		this.tc = tc;
	}
	
	public abstract void init();
	public abstract void update();
	public abstract void render();
	public abstract void cleanup();
}
