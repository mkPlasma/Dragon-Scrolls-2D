package engine.screens;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import engine.KeyboardListener;
import engine.graphics.Renderer;
import engine.graphics.TextureCache;
import engine.objects.Map;
import engine.objects.NPC;
import engine.objects.Player;
import engine.objects.Tile;
import engine.ui.menus.InventoryMenu;
import engine.ui.menus.Menu;
import engine.ui.menus.StatsMenu;

public class MainScreen extends GameScreen{
	
	// Current map
	private Map map;
	
	// Player
	private Player player;
	
	private ArrayList<NPC> npcs;
	
	// Menus
	private ArrayList<Menu> menus;
	
	private InventoryMenu invMenu;
	private StatsMenu statsMenu;
	
	private boolean menuOpen;
	
	// Camera position
	private int camX, camY;
	
	public MainScreen(ScreenManager manager, Renderer r, TextureCache tc){
		super(manager, r, tc);
	}
	
	public void init(){
		r.initMainScreen();
		
		// temp init player, 
		player = new Player(8, 8, Player.CLASS_KNIGHT, tc);
		
		// Initialize menus
		menus = new ArrayList<Menu>(2);
		invMenu = new InventoryMenu(player, tc);
		statsMenu = new StatsMenu(invMenu, tc);
		menus.add(invMenu);
		menus.add(statsMenu);
		
		// temp init map
		Tile[] g = {new Tile(Tile.TILE_GRASS, tc)};
		Tile[] s = {new Tile(Tile.TILE_GRAVEL, tc)};
		Tile[] e = {new Tile(-1, tc)};
		
		/*map = new Map(
			new Tile[][][]{
				{g, g, g, e, e, e, g, g, g, e, e, e, g, g, g},
				{g, e, g, g, g, g, g, s, g, g, g, g, g, e, g},
				{g, g, g, e, e, e, g, g, g, e, e, e, g, g, g},
				{e, g, e, e, e, e, e, g, e, e, e, e, e, g, e},
				{e, g, e, e, g, g, g, g, g, g, g, e, e, g, e},
				{e, g, e, e, g, s, s, s, s, s, g, e, e, g, e},
				{g, g, g, e, g, s, e, e, e, s, g, e, g, g, g},
				{g, s, g, g, g, s, e, e, e, s, g, g, g, s, g},
				{g, g, g, e, g, s, e, e, e, s, g, e, g, g, g},
				{e, g, e, e, g, s, s, s, s, s, g, e, e, g, e},
				{e, g, e, e, g, g, g, g, g, g, g, e, e, g, e},
				{e, g, e, e, e, e, e, g, e, e, e, e, e, g, e},
				{g, g, g, e, e, e, g, g, g, e, e, e, g, g, g},
				{g, e, g, g, g, g, g, s, g, g, g, g, g, e, g},
				{g, g, g, e, e, e, g, g, g, e, e, e, g, g, g},
			}
		);*/
		map = new Map(
			new Tile[][][]{
				{g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g},
				{g, s, s, s, g, g, g, s, s, s, g, g, g, s, s, s, g},
				{g, s, g, s, s, s, s, s, g, s, s, s, s, s, g, s, g},
				{g, s, s, s, g, g, g, s, s, s, g, g, g, s, s, s, g},
				{g, g, s, g, g, g, g, g, s, g, g, g, g, g, s, g, g},
				{g, g, s, g, g, s, s, s, s, s, s, s, g, g, s, g, g},
				{g, g, s, g, g, s, e, e, g, e, e, s, g, g, s, g, g},
				{g, s, s, s, g, s, e, e, g, e, e, s, g, s, s, s, g},
				{g, s, g, s, s, s, g, g, g, g, g, s, s, s, g, s, g},
				{g, s, s, s, g, s, e, e, g, e, e, s, g, s, s, s, g},
				{g, g, s, g, g, s, e, e, g, e, e, s, g, g, s, g, g},
				{g, g, s, g, g, s, s, s, s, s, s, s, g, g, s, g, g},
				{g, g, s, g, g, g, g, g, s, g, g, g, g, g, s, g, g},
				{g, s, s, s, g, g, g, s, s, s, g, g, g, s, s, s, g},
				{g, s, g, s, s, s, s, s, g, s, s, s, s, s, g, s, g},
				{g, s, s, s, g, g, g, s, s, s, g, g, g, s, s, s, g},
				{g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g},
			}
		);
		
		npcs.add(new NPC(0, 0, tc));
		
		/*
		int d1 = map.getDiagonalLength(false);
		int d2 = map.getDiagonalLength(true);
		
		camX = (640 - d1*32)/2 + (int)((d1/2f)*32);
		camY = (480 - d2*8)/2 - (int)((d2/2f)*8);
		*/
	}
	
	public void update(){
		updateKeypresses();
		updateMenus();
		
		updateMap();
	}
	
	private void updateKeypresses(){
		
		// Inventory
		if(KeyboardListener.isKeyPressed(GLFW.GLFW_KEY_E)){
			if(!invMenu.isOpen()){
				invMenu.open();
				
				statsMenu.updateData(player);
			}
			else
				invMenu.close();
		}
	}
	
	private void updateMenus(){
		
		menuOpen = false;
		
		for(Menu m:menus){
			m.update();
			
			if(m.isOpen())
				menuOpen = true;
		}
		
		if(invMenu.updateStats())
			statsMenu.updateData(player);
	}
	
	private void updateMap(){
		
		if(menuOpen)
			return;
		
		updatePlayer();
		updateNPCs();
		updateCameraPos();
	}
	
	private void updatePlayer(){
		
		// Set allowed movements
		boolean[] movementsAllowed = new boolean[4];
		
		int px = player.getTileX();
		int py = player.getTileY();
		int pz = player.getTileZ();
		
		Tile[][][] tiles = map.getTiles();

		movementsAllowed[0] = px < tiles[py].length - 1	&& !tiles[py][px + 1][pz].isSolid();
		movementsAllowed[1] = px > 0					&& !tiles[py][px - 1][pz].isSolid();
		movementsAllowed[2] = py < tiles.length - 1		&& !tiles[py + 1][px][pz].isSolid();
		movementsAllowed[3] = py > 0					&& !tiles[py - 1][px][pz].isSolid();
		
		// Update
		player.setMovementsAllowed(movementsAllowed);
		player.update();
	}
	
	private void updateNPCs(){
		for(NPC n:npcs)
			n.update();
	}
	
	private void updateCameraPos(){
		float x = player.getX();
		float y = player.getY();
		
		camX = (int)(320 - (x - y)*16);
		camY = (int)(240 - (x + y)*8);
	}
	
	
	
	public void render(){
		r.setCamPos(camX, camY);
		r.updateTiles(map);
		r.updatePlayer(player);
		r.updateMenus(menus);
		
		r.render();
	}

	public void cleanup(){
		r.cleanup();
	}
}
