package engine;

// Required since java.awt.Color hangs program on mac

public class Color{
	
	public static final Color
		RED		= new Color(255, 0, 0),
		GREEN	= new Color(0, 255, 0),
		BLUE	= new Color(0, 0, 255),
		
		BROWN	= new Color(128, 64, 0),
		
		BLACK		= new Color(0, 0, 0),
		DARK_GRAY	= new Color(64, 64, 64),
		GRAY		= new Color(128, 128, 128),
		LIGHT_GRAY	= new Color(192, 192, 192),
		WHITE		= new Color(255, 255, 255);
	
	private int r, g, b;
	
	public Color(int r, int g, int b){
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	public int getRed(){
		return r;
	}
	
	public int getGreen(){
		return g;
	}
	
	public int getBlue(){
		return b;
	}
}
