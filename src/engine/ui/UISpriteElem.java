package engine.ui;

import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public abstract class UISpriteElem extends UIElement{
	
	protected Sprite sprite;
	
	public UISpriteElem(float x, float y, TextureCache tc){
		super(x, y, tc);
	}
	
	public Sprite getSprite(){
		return sprite;
	}
}
