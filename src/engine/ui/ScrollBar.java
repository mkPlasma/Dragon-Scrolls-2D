package engine.ui;

import engine.Color;
import engine.graphics.TextureCache;

public class ScrollBar extends Bar{
	
	public ScrollBar(float x, float y, int length, TextureCache tc){
		super(x, y, length, Color.WHITE, tc);
		
		genParts();
	}
	
	private void genParts(){
		
		parts.clear();
		
		// Bar background
		parts.add(new Icon(Icon.ICON_SCROLL_BAR, x - 4, y, 1, true, false, false, tc));
		parts.add(new Icon(Icon.ICON_SCROLL_BAR, x - 4, y + 8 + (length - 32)/2f, (length - 32)/8f, true, true, false, tc));
		parts.add(new Icon(Icon.ICON_SCROLL_BAR, x - 4, y + length - 16, -1, true, false, false, tc));
		
		// Bar
		parts.add(new Icon(Icon.ICON_SCROLL_BAR, x - 4, y + 1, 1, true, false, true, tc));
	}
	
	public void update(float fill, float offset){
		
		float fLength = (length - 2)*fill;
		
		parts.get(3).getSprite().setScaleY(fLength/16f);
		parts.get(3).setY(y - 4 + (fLength/2f) + (length - fLength - 8)*offset);
		
		// 0 - 1
		// 1 - length - fLength - 1
	}
}
