package engine.ui;

import java.util.ArrayList;

import engine.Color;
import engine.graphics.TextureCache;

public class Bar extends UIElement{
	
	protected ArrayList<Icon> parts;
	
	protected int length;
	
	protected Color color;

	public Bar(float x, float y, int length, Color color, TextureCache tc){
		super(x, y, tc);
		
		this.length = length;
		this.color = color;
		
		parts = new ArrayList<Icon>(6);
		
		if(!(this instanceof ScrollBar))
			genParts();
	}
	
	private void genParts(){
		
		parts.clear();
		
		// Empty bar
		parts.add(new Icon(Icon.ICON_BAR_EMPTY, x, y, tc));
		parts.add(new Icon(Icon.ICON_BAR_EMPTY, x + 8 + ((length - 32)/2f), y, (length - 32)/8f, true, tc));
		parts.add(new Icon(Icon.ICON_BAR_EMPTY, x - 16 + length, y, -1, false, tc));
		
		// Filled bar
		parts.add(new Icon(Icon.ICON_BAR, x, y, color, tc));
		parts.add(new Icon(Icon.ICON_BAR, x + ((length - 32)/2f), y, (length - 32)/8f, true, color, tc));
		parts.add(new Icon(Icon.ICON_BAR, x - 16 + length, y, -1, false, color, tc));
	}
	
	public void update(float fill){
		
		if(fill < 0)
			fill = 0;
		else if(fill >= 1)
			fill = 1;
		
		int fLength = (int)(length*fill);
		int w = Math.min(fLength, 16);
		Icon p = parts.get(3);

		if(w == 0)
			p.setVisible(false);
		else{
			p.setVisible(true);
			p.getSprite().setWidth(w);
			p.getSprite().genTextureCoords();
			p.setX(x - 8 + w/2f);
		}
		
		
		
		w = Math.max(Math.min(fLength - 16, length - 32), 0);
		p = parts.get(4);
		
		if(w == 0)
			p.setVisible(false);
		else{
			p.setVisible(true);
			p.getSprite().setScaleX(w/8f);
			p.setX(x + 8 + w/2f);
		}
		
		
		
		w = Math.max(fLength - (length - 16), 0);
		p = parts.get(5);
		
		if(w == 0)
			p.setVisible(false);
		else{
			p.setVisible(true);
			p.getSprite().setX(96 - w);
			p.getSprite().setWidth(w);
			p.getSprite().genTextureCoords();
			p.setX(x - 24 + length + w/2f);
		}
	}
	
	public void setLength(int length){
		this.length = length;
		genParts();
	}
	
	public ArrayList<Icon> getParts(){
		return parts;
	}
}
