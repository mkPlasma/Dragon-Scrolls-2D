package engine.ui.menus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import engine.Color;
import engine.graphics.TextureCache;
import engine.objects.Player;
import engine.objects.items.StatusEffect;
import engine.ui.Bar;
import engine.ui.Icon;
import engine.ui.Text;
import engine.ui.TextArea;

import static engine.ui.Icon.*;

public class StatsMenu extends Menu{
	
	private Icon classIcon;
	private Text levelText, xpText, hpText, mpText, defText, gpText;
	private Bar xpBar, hpBar, mpBar;
	
	private ArrayList<Icon> statusIcons;
	
	private DecimalFormat df;
	
	public StatsMenu(Menu link, TextureCache tc){
		super(link, tc);
		init();
	}
	
	public StatsMenu(TextureCache tc){
		super(tc);
		init();
	}
	
	public void init(){

		// Stats area
		textAreas.add(new TextArea(8, 8, 272, 192, true, 1, tc));
		
		texts.add(new Text(64, 24, "Stats", -1, Color.BLACK, 1, tc));
		
		levelText = new Text(64, 48, "Level", -1, Color.BLACK, 1, tc);
		texts.add(levelText);
		
		texts.add(new Text(64, 78, "XP: HP: MP:", 1, Color.BLACK, 1, tc));
		
		xpText = new Text(0, 78, "", -1, Color.BLACK, 1, tc);
		hpText = new Text(0, 100, "", -1, Color.BLACK, 1, tc);
		mpText = new Text(0, 122, "", -1, Color.BLACK, 1, tc);
		texts.add(xpText);
		texts.add(hpText);
		texts.add(mpText);
		
		defText = new Text(64, 146, "DEF: ", -1, Color.BLACK, 1, tc);
		gpText = new Text(184, 146, "Gold: ", + -1, Color.BLACK, 1, tc);
		texts.add(defText);
		texts.add(gpText);
		
		df = new DecimalFormat("###,###");
		
		classIcon = new Icon(ICON_KNIGHT, 46, 48, tc);
		icons.add(classIcon);
		icons.add(new Icon(ICON_XP,		46, 78, tc));
		icons.add(new Icon(ICON_HEART,		46, 100, tc));
		icons.add(new Icon(ICON_MANA,		46, 122, tc));
		icons.add(new Icon(ICON_SHIELD,	46, 146, new Color[]{Color.LIGHT_GRAY, Color.DARK_GRAY}, tc));
		icons.add(new Icon(ICON_GOLD,		164, 146, tc));
		
		
		xpBar = new Bar(96, 78, 64, Color.GREEN, tc);
		hpBar = new Bar(96, 100, 64, Color.RED, tc);
		mpBar = new Bar(96, 122, 64, Color.BLUE, tc);
		bars.add(xpBar);
		bars.add(hpBar);
		bars.add(mpBar);
		
		
		statusIcons = new ArrayList<Icon>();
		
		setElementsVisible(false);
	}
	
	public void updateData(Player player){
		
		// Stats
		classIcon.setType(ICON_KNIGHT + player.getClassType());
		
		levelText.setText("Level " + player.getLevel() + " " + player.getClassName());

		int hpAdd = player.getMaxHP() - player.getBaseMaxHP();
		int mpAdd = player.getMaxMP() - player.getBaseMaxMP();
		
		xpText.setText(player.getXP() + "/" + player.getXPReq());
		hpText.setText(player.getHP() + "/" + player.getMaxHP() + (hpAdd > 0 ? "(+" + hpAdd + ")" : ""));
		mpText.setText(player.getMP() + "/" + player.getMaxMP() + (mpAdd > 0 ? "(+" + mpAdd + ")" : ""));

		int xpx = 304 - xpText.getText().length()*9;
		int hpx = 304 - hpText.getText().length()*9;
		int mpx = 304 - mpText.getText().length()*9;
		xpText.setX(xpx);
		hpText.setX(hpx);
		mpText.setX(mpx);
		
		defText.setText("DEF:" + player.getDef());
		gpText.setText("Gold:" + df.format(player.getGold()));

		xpBar.setLength((int)(xpx - xpBar.getX()));
		hpBar.setLength((int)(hpx - hpBar.getX()));
		mpBar.setLength((int)(mpx - mpBar.getX()));
		xpBar.update((float)player.getXP()/player.getXPReq());
		hpBar.update((float)player.getHP()/player.getMaxHP());
		mpBar.update((float)player.getMP()/player.getMaxMP());
		
		
		for(Icon i:statusIcons)
			i.delete();
		
		statusIcons.clear();
		
		ArrayList<StatusEffect> se = player.getStatusEffects();
		
		for(int i = 0; i < se.size(); i++){
			StatusEffect s = se.get(i);
			statusIcons.add(new Icon(s.getIconType(), 64 + 20*i, 166, s.getIconColors(), tc));
		}
		
		icons.addAll(statusIcons);
	}
}