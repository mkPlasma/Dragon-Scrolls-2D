package engine.ui.menus;

import java.util.ArrayList;

import engine.graphics.TextureCache;
import engine.ui.Bar;
import engine.ui.Icon;
import engine.ui.Text;
import engine.ui.TextArea;

public abstract class Menu{

	protected ArrayList<TextArea> textAreas;
	protected ArrayList<Text> texts;
	protected ArrayList<Icon> icons;
	protected ArrayList<Bar> bars;
	
	protected boolean open;
	protected boolean opening;
	protected boolean closing;
	
	protected Menu link;
	
	protected TextureCache tc;
	
	public Menu(TextureCache tc){
		this(null, tc);
	}
	
	public Menu(Menu link, TextureCache tc){
		this.link = link;
		this.tc = tc;
		
		textAreas = new ArrayList<TextArea>();
		texts = new ArrayList<Text>();
		icons = new ArrayList<Icon>();
		bars = new ArrayList<Bar>();
	}
	
	public void open(){
		
		if(closing)
			return;
		
		for(TextArea t:textAreas)
			t.open();
		
		opening = true;
	}
	
	public void update(){
		
		if(link != null){
			if(!opening && link.isOpening())
				open();
			
			if(!closing && link.isClosing())
				close();
		}
		
		for(int i = 0; i < textAreas.size(); i++){
			TextArea t = textAreas.get(i);
			
			if(t.isDeleted()){
				textAreas.remove(i);
				i--;
				continue;
			}
			
			if(isOpen())
				t.update();
		}
		
		// Remove elements if necessary
		for(int i = 0; i < texts.size(); i++){
			if(texts.get(i).isDeleted()){
				texts.remove(i);
				i--;
			}
		}
		
		for(int i = 0; i < icons.size(); i++){
			if(icons.get(i).isDeleted()){
				icons.remove(i);
				i--;
			}
		}
		
		for(int i = 0; i < bars.size(); i++){
			if(bars.get(i).isDeleted()){
				bars.remove(i);
				i--;
			}
		}
		
		// Check if open
		if(opening){
			open = true;
			
			for(TextArea t:textAreas)
				if(!t.isOpen())
					open = false;
			
			if(link != null)
				open = open && link.isFullyOpen();
			
			if(open){
				opening = false;
				
				setElementsVisible(true);
			}
		}
		
		if(closing){
			open = false;

			for(TextArea t:textAreas)
				if(t.isOpen())
					open = true;
			
			if(link != null)
				open = open || link.isFullyOpen();
			
			if(!open)
				closing = false;
		}
	}
	
	public void close(){
		
		if(!open)
			return;
		
		for(TextArea t:textAreas)
			t.close();
		
		setElementsVisible(false);
		
		closing = true;
	}
	
	protected void setElementsVisible(boolean visible){
		for(Text t:texts)
			t.setVisible(visible);
		
		for(Icon i:icons)
			i.setVisible(visible);
		
		for(Bar b:bars)
			b.setVisible(visible);
	}
	
	public boolean isOpen(){
		return open || opening;
	}
	
	public boolean isFullyOpen(){
		return open;
	}
	
	public boolean isOpening(){
		return opening;
	}
	
	public boolean isClosing(){
		return closing;
	}
	
	public ArrayList<TextArea> getTextAreas(){
		return textAreas;
	}
	
	public ArrayList<Text> getTexts(){
		return texts;
	}
	
	public ArrayList<Icon> getIcons(){
		return icons;
	}
	
	public ArrayList<Bar> getBars(){
		return bars;
	}
}
