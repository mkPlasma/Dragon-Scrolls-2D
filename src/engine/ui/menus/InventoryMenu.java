package engine.ui.menus;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import engine.Color;
import engine.KeyboardListener;
import engine.graphics.TextureCache;
import engine.objects.Player;
import engine.objects.items.Attack;
import engine.objects.items.ConsumableItem;
import engine.objects.items.EquippableItem;
import engine.objects.items.Item;
import engine.objects.items.StatusEffect;
import engine.objects.items.Weapon;
import engine.ui.Icon;
import engine.ui.ScrollBar;
import engine.ui.Text;
import engine.ui.TextArea;

import static engine.ui.Icon.*;

public class InventoryMenu extends Menu{
	
	private static final int
		MENU_INVENTORY	= 0,
		MENU_OPTIONS	= 1,
		MENU_STATS		= 2,
		MENU_ATTACKS	= 3,
		MENU_DROP		= 4,
		
		ITEM_USE		= 0,
		ITEM_EQUIP		= 1,
		ITEM_STATS		= 2,
		ITEM_ATTACKS	= 3,
		ITEM_DROP		= 4;
	
	private static final int INV_MENU_SIZE = 11;
	
	
	private boolean firstUpdate;
	
	private Player player;
	
	// Equipment
	private Icon weaponIcon;
	private Icon specialWeaponIcon;
	private Icon armorIcon;
	private Text equipText;
	
	// Inventory item list
	private Text invItemCount;
	private ArrayList<Item> inventory;
	private ArrayList<Integer> inventoryCounts;
	private ArrayList<Icon> invIcons;
	private ArrayList<Text> invTexts;
	
	// Cursor
	private Icon cursor, cursor2;
	private int invCursor, optCursor, attCursor, dropCursor;
	private int invScroll;
	
	// Inventory scroll bar
	private ScrollBar invScrollBar;
	
	private int currentMenu;
	
	// True if inventory is empty and no equipment
	private boolean inventoryEmpty;
	
	// Current item info
	private Icon itemIcon;
	private Text itemName, itemTierOptions, itemDescription;
	
	// Current item options
	private ArrayList<Integer> itemOptions;
	
	// Current item stats
	private ArrayList<Icon> statIcons;
	private ArrayList<Text> statTexts;
	
	// For attack list/dropping items
	private ArrayList<Icon> extraArrows;
	
	// Total count of current item (for dropping)
	private int itemTotalCount;
	
	// Update stats window if necessary
	private boolean updateStats;
	
	public InventoryMenu(Player player, TextureCache tc){
		super(tc);
		init();
		
		this.player = player;
		
		// Items
		inventory = player.getInventory();
		inventoryCounts = player.getInventoryCounts();
		
	}
	
	public void init(){
		
		textAreas.add(new TextArea(352, 8, 256, 400, false, 1, tc));
		
		// Equipment
		texts.add(new Text(384, 48, "Equipment", -1, Color.BLACK, 1, tc));
		
		equipText = new Text(404, 80, "", -1, Color.BLACK, 1, tc);
		texts.add(equipText);
		
		weaponIcon			= new Icon(0, 388, 80, tc);
		specialWeaponIcon	= new Icon(0, 388, 102, tc);
		armorIcon			= new Icon(0, 388, 126, tc);
		icons.add(weaponIcon);
		icons.add(specialWeaponIcon);
		icons.add(armorIcon);
		
		
		// Items
		
		texts.add(new Text(384, 168, "Inventory", -1, Color.BLACK, 1, tc));
		
		invItemCount = new Text(512, 168, "", -1, Color.BLACK, 1, tc);
		texts.add(invItemCount);
		
		cursor = new Icon(ICON_CURSOR_R, 368, 0, tc);
		cursor2 = new Icon(ICON_CURSOR_R, 48, 278, tc);
		icons.add(cursor);
		icons.add(cursor2);
		
		invScrollBar = new ScrollBar(588, 192, 240, tc);
		bars.add(invScrollBar);
		
		invIcons = new ArrayList<Icon>();
		invTexts = new ArrayList<Text>();
		
		
		
		
		// Item info
		textAreas.add(new TextArea(8, 236, 272, 212, true, 1, tc));
		
		itemIcon = new Icon(0, 46, 256, tc);
		icons.add(itemIcon);
		
		itemName = new Text(66, 256, "", -1, Color.BLACK, 1, tc);
		texts.add(itemName);
		
		itemTierOptions = new Text(66, 278, "", 25, Color.BLACK, 1, tc);
		texts.add(itemTierOptions);
		
		itemDescription = new Text(66, 310, "", 25, Color.BLACK, 1, tc);
		texts.add(itemDescription);

		statIcons = new ArrayList<Icon>();
		statTexts = new ArrayList<Text>();
		
		itemOptions = new ArrayList<Integer>();

		extraArrows = new ArrayList<Icon>();
		
		setElementsVisible(false);
	}
	
	public void open(){
		super.open();
		firstUpdate = true;
		currentMenu = MENU_INVENTORY;
	}
	
	public void update(){
		
		super.update();
		
		if(!isOpen())
			return;
		
		if(!open || closing)
			return;
		
		if(firstUpdate){
			genItemList();
			updateItemList();
			updateEquipment();
			updateCursor();
			cursor2.setVisible(false);
			firstUpdate = false;
		}
		
		if(inventoryEmpty){
			cursor.setVisible(false);
			return;
		}
		
		// Cursor
		if(KeyboardListener.isKeyPressed(GLFW.GLFW_KEY_DOWN)){
			switch(currentMenu){
				
				case MENU_INVENTORY:
					int c = invCursor;
					
					if(invCursor < inventory.size() - 1)
						invCursor++;
					
					// Equipment select, skip slot if empty
					if(invCursor < 0){
						if(invCursor == -2 && player.getSpecialWeapon() == null)
							invCursor++;
						
						if(invCursor == -1 && player.getArmor() == null)
							invCursor++;
					}
					
					if(invCursor == 0 && inventory.isEmpty()){
						invCursor = c;
						break;
					}
					
					// Wrap to top
					/*if(invCursor == inventory.size()){
						invCursor = 0;
						invScroll = 0;
						updateItemList();
					}*/
					
					// Scroll down if necessary
					else if(invCursor >= invScroll + INV_MENU_SIZE){
						invScroll++;
						updateItemList();
					}
					
					updateItemInfo();
					break;
				
				case MENU_OPTIONS:
					optCursor++;
					
					// Wrap to top
					if(optCursor == itemOptions.size())
						optCursor = 0;
					
					break;
				
				case MENU_ATTACKS:
					int n = ((Weapon)currentItem()).getAttacks().length - 1;
					
					if(attCursor < n){
						attCursor++;
						updateAttackInfo();
						
						if(attCursor == n)
							extraArrows.get(1).setVisible(false);
						
						extraArrows.get(0).setVisible(true);
					}
					break;
				
				case MENU_DROP:
					// Yes/No
					if(itemTotalCount == 1)
						dropCursor = dropCursor == 0 ? 1 : 0;
					
					// Counter
					else if(dropCursor > 1){
						dropCursor--;
						itemTierOptions.setText("\n" + dropCursor);
						
						if(dropCursor == 1)
							extraArrows.get(1).setVisible(false);
						extraArrows.get(0).setVisible(true);
					}
					
					break;
			}
			
			updateCursor();
		}
		else if(KeyboardListener.isKeyPressed(GLFW.GLFW_KEY_UP)){
			switch(currentMenu){
				
				case MENU_INVENTORY:
					if(invCursor > -3)
						invCursor--;

					// Equipment select, skip slot if empty
					if(invCursor < 0){
						int c = invCursor + 1;
						
						if(invCursor == -1 && player.getArmor() == null)
							invCursor--;
						
						if(invCursor == -2 && player.getSpecialWeapon() == null)
							invCursor--;
						
						if(invCursor == -3 && player.getWeapon() == null)
							invCursor--;
						
						if(invCursor == -4)
							invCursor = c;
						
						// Wrap to bottom if all slots are empty
						/*if(invCursor == -4){
							if(player.getArmor() == null && player.getSpecialWeapon() == null && player.getWeapon() == null){
								invCursor = inventory.size() - 1;
								invScroll = invCursor - (INV_MENU_SIZE - 1);
								updateItemList();
							}
							else
								invCursor = c;
						}*/
					}
					
					// Scroll up if necessary
					else if(invCursor < invScroll){
						invScroll--;
						updateItemList();
					}
					
					updateItemInfo();
					break;
				
				case MENU_OPTIONS:
					optCursor--;
					
					// Wrap to bottom
					if(optCursor == -1)
						optCursor = itemOptions.size() - 1;
					break;
				
				case MENU_ATTACKS:
					if(attCursor > 0){
						attCursor--;
						updateAttackInfo();
						
						if(attCursor == 0)
							extraArrows.get(0).setVisible(false);
						
						extraArrows.get(1).setVisible(true);
					}
					
					break;
				
				case MENU_DROP:
					// Yes/No
					if(itemTotalCount == 1)
						dropCursor = dropCursor == 0 ? 1 : 0;
					
					// Counter
					else if(dropCursor < itemTotalCount){
						dropCursor++;
						itemTierOptions.setText("\n" + dropCursor);
						
						if(dropCursor == itemTotalCount)
							extraArrows.get(0).setVisible(false);
						extraArrows.get(1).setVisible(true);
					}
					
					break;
			}
			
			updateCursor();
		}
		
		// Item options
		if(KeyboardListener.isKeyPressed(GLFW.GLFW_KEY_Z)){
			switch(currentMenu){
				
				case MENU_INVENTORY:
					updateItemOptions(true);
					updateCursor();
					break;
				
				case MENU_OPTIONS:
					itemOption(itemOptions.get(optCursor));
					break;
				
				case MENU_STATS:
					itemStats(false);
					break;
				
				case MENU_ATTACKS:
					itemAttacks(false);
					break;
				
				case MENU_DROP:
					if(itemTotalCount == 1){
						if(dropCursor == 0){

							Item item = currentItem();
							
							if(invCursor >= 0)
								player.removeItem(item);
							else{
								player.unequip(item instanceof Weapon ? ((Weapon)item).isSpecialWeapon() ? 1 : 0 : 2);
								updateEquipment();
							}
							
							genItemList();
							updateItemList();
							itemDrop(false);
							updateItemOptions(false);
							updateCursor();
						}
						else
							itemDrop(false);
					}
					else{
						player.removeItem(currentItem(), dropCursor);
						genItemList();
						updateItemList();
						itemDrop(false);
						updateItemOptions(false);
						updateCursor();
					}
					
					break;
			}
		}
		
		if(KeyboardListener.isKeyPressed(GLFW.GLFW_KEY_X)){
			switch(currentMenu){
				
				case MENU_INVENTORY:
					close();
					break;
				
				case MENU_OPTIONS:
					updateItemOptions(false);
					updateCursor();
					break;
				
				case MENU_STATS:
					itemStats(false);
					break;
				
				case MENU_ATTACKS:
					itemAttacks(false);
					break;
				
				case MENU_DROP:
					itemDrop(false);
					break;
			}
		}
	}
	
	public void close(){
		switch(currentMenu){
			
			case MENU_OPTIONS:
				updateItemOptions(false);
				break;
			
			case MENU_STATS:
				itemStats(false);
				updateItemOptions(false);
				break;
			
			case MENU_DROP:
				itemDrop(false);
				updateItemOptions(false);
				break;
		}
		
		for(Icon i:invIcons) i.delete();
		for(Text t:invTexts) t.delete();
		invIcons.clear();
		invTexts.clear();
		
		super.close();
	}
	
	private void updateCursor(){
		switch(currentMenu){
			
			case MENU_INVENTORY:
				cursor.setY(invCursor >= 0 ? 196 + (invCursor - invScroll)*22 : 124 + (invCursor + 1)*22);
				return;
			
			case MENU_OPTIONS:
				cursor2.setY(278 + optCursor*22);
				return;
			
			 case MENU_DROP:
				 cursor2.setY(278 + dropCursor*22);
				return;
		}
	}
	
	private void updateEquipment(){
		
		Weapon weapon = player.getWeapon();
		Weapon specialWeapon = player.getSpecialWeapon();
		EquippableItem armor = player.getArmor();
		
		equipText.setText(
			(weapon			== null ? "No weapon"			: weapon.getName()) + "\n" +
			(specialWeapon	== null ? "No special weapon"	: specialWeapon.getName()) + "\n" +
			(armor			== null ? "No armor"			: armor.getName())
		);
		
		if(weapon == null){
			weaponIcon.setType(ICON_SWORD + player.getClassType());
			weaponIcon.setColors(new Color[]{Color.DARK_GRAY});
		}
		else{
			weaponIcon.setType(weapon.getIconType());
			weaponIcon.setColors(weapon.getColors());
		}
		
		if(specialWeapon == null){
			specialWeaponIcon.setType(ICON_SHIELD + player.getClassType());
			specialWeaponIcon.setColors(new Color[]{Color.DARK_GRAY});
		}
		else{
			specialWeaponIcon.setType(specialWeapon.getIconType());
			specialWeaponIcon.setColors(specialWeapon.getColors());
		}
		
		if(armor == null){
			armorIcon.setType(ICON_KNIGHT_ARMOR + player.getClassType());
			armorIcon.setColors(new Color[]{Color.DARK_GRAY});
		}
		else{
			armorIcon.setType(armor.getIconType());
			armorIcon.setColors(armor.getColors());
		}
	}
	
	private void genItemList(){
		
		for(Icon i:invIcons) i.delete();
		for(Text t:invTexts) t.delete();
		invIcons.clear();
		invTexts.clear();

		invCursor = 0;
		invScroll = 0;
		
		inventoryEmpty = false;
		if(inventory.isEmpty()){
			invItemCount.setText("0/" + player.getInventorySize());
			
			invTexts.add(new Text(400, 196, "Empty", -1, Color.BLACK, 1, tc));
			for(Text t:invTexts) t.setVisible(open);
			texts.addAll(invTexts);
			
			if(player.getWeapon() != null)
				invCursor = -3;
			else if(player.getSpecialWeapon() != null)
				invCursor = -2;
			else if(player.getArmor() != null)
				invCursor = -1;
			else
				inventoryEmpty = true;
			
			updateItemInfo();
			return;
		}
		
		updateCursor();
		
		invItemCount.setText(inventory.size() + "/" + player.getInventorySize());
		invItemCount.setX(592 - invItemCount.getText().length()*9);
		
		for(int i = 0; i < inventory.size(); i++){
			Item it = inventory.get(i);
			String c = Integer.toString(inventoryCounts.get(i));
			
			// Inventory icon, item name, item count
			invIcons.add(new Icon(it.getIconType(), 388, 0, it.getColors(), tc));
			invTexts.add(new Text(404, 0, it.getName(), -1, Color.BLACK, 1, tc));
			invTexts.add(new Text(576 - c.length()*9, 0, c, -1, Color.BLACK, 1, tc));
		}
		
		updateItemInfo();
		
		for(Icon i:invIcons) i.setVisible(open);
		for(Text t:invTexts) t.setVisible(open);
		icons.addAll(invIcons);
		texts.addAll(invTexts);
	}
	
	private void updateItemList(){
		
		int size = inventory.size();
		
		if(size <= INV_MENU_SIZE)
			invScrollBar.setVisible(false);
		else{
			invScrollBar.update((float)INV_MENU_SIZE/size, invScroll/((float)size - INV_MENU_SIZE));
			invScrollBar.setVisible(true);
		}
		
		for(int i = 0; i < size; i++){
			
			Icon icon = invIcons.get(i);
			Text name = invTexts.get(i*2);
			Text count = invTexts.get(i*2 + 1);
			
			if(i < invScroll || i >= invScroll + INV_MENU_SIZE){
				icon.setVisible(false);
				name.setVisible(false);
				count.setVisible(false);
				continue;
			}
			
			icon.setVisible(true);
			name.setVisible(true);
			count.setVisible(true);
			
			int y = 196 + 22*(i - invScroll);
			
			icon.setY(y);
			name.setY(y);
			count.setY(y);
		}
	}
	
	private void updateItemInfo(){
		
		Item i = currentItem();
		
		for(Icon ic:statIcons) ic.delete();
		for(Text t:statTexts) t.delete();
		statIcons.clear();
		statTexts.clear();
		
		if(i == null){
			itemIcon.setVisible(false);
			itemName.setVisible(false);
			itemTierOptions.setVisible(false);
			itemDescription.setVisible(false);
			return;
		}

		itemIcon.setVisible(open);
		itemName.setVisible(open);
		itemTierOptions.setVisible(open);
		itemDescription.setVisible(open);
		
		itemIcon.setType(i.getIconType());
		itemIcon.setColors(i.getColors());
		
		itemName.setText(i.getName());
		
		String tier = "";
		
		if(i.getTier() > 0)
			tier += "Tier " + i.getTier();
		
		if(i instanceof Weapon)
			tier += " " + Player.getClassName(((EquippableItem)i).getClassType()) + " weapon";
		else if(i instanceof EquippableItem)
			tier += " " + Player.getClassName(((EquippableItem)i).getClassType()) + " armor";
		
		else if(i instanceof ConsumableItem)
			tier += " potion";
		
		itemTierOptions.setText(tier);
		itemDescription.setText(i.getDescription());

		if(i instanceof EquippableItem){
			EquippableItem e = (EquippableItem)i;
			
			int def = e.getDef();
			int hp = e.getMaxHP();
			int mp = e.getMaxMP();
			
			float x = 66;
			float y = 408;
			
			if(e instanceof Weapon){
				statIcons.add(new Icon(e.getIconType(), x, y, e.getColors(), tc));
				statTexts.add(new Text(x + 16, y, Integer.toString(((Weapon)e).getAttacks().length), -1, Color.BLACK, 1, tc));
				x += 64;
			}
			
			if(def != 0){
				statIcons.add(new Icon(ICON_SHIELD, x, y, new Color[]{Color.LIGHT_GRAY, Color.DARK_GRAY}, tc));
				statTexts.add(new Text(x + 16, y, (def > 0 ? "+" : "") + Integer.toString(def), -1, Color.BLACK, 1, tc));
				x += 64;
			}
			if(hp != 0){
				statIcons.add(new Icon(ICON_HEART, x, y, tc));
				statTexts.add(new Text(x + 16, y, (hp > 0 ? "+" : "") + Integer.toString(hp), -1, Color.BLACK, 1, tc));
				x += 64;
			}
			if(mp != 0){
				statIcons.add(new Icon(ICON_MANA, x, y, tc));
				statTexts.add(new Text(x + 16, y, (mp > 0 ? "+" : "") + Integer.toString(mp), -1, Color.BLACK, 1, tc));
				x += 64;
			}
		}
		else if(i instanceof ConsumableItem){
			ConsumableItem c = (ConsumableItem)i;
			
			int hp = c.getHP();
			int mp = c.getMP();
			
			float x = 66;
			float y = 408;
			
			if(hp != 0){
				statIcons.add(new Icon(ICON_HEART, x, y, tc));
				statTexts.add(new Text(x + 16, y, (hp > 0 ? "+" : "") + Integer.toString(hp), -1, Color.BLACK, 1, tc));
				x += 64;
			}
			if(mp != 0){
				statIcons.add(new Icon(ICON_MANA, x, y, tc));
				statTexts.add(new Text(x + 16, y, (mp > 0 ? "+" : "") + Integer.toString(mp), -1, Color.BLACK, 1, tc));
				x += 64;
			}
			
			StatusEffect[] se = c.getStatusEffects();
			
			if(se != null){
				for(StatusEffect s:se){
					statIcons.add(new Icon(s.getIconType(), x, y, s.getIconColors(), tc));
					statTexts.add(new Text(x + 16, y, Integer.toString(s.getLevel()), -1, Color.BLACK, 1, tc));
					x += 64;
				}
			}
		}
		
		for(Icon ic:statIcons) ic.setVisible(open);
		for(Text t:statTexts) t.setVisible(open);
		icons.addAll(statIcons);
		texts.addAll(statTexts);
	}
	
	private void updateItemOptions(boolean inMenu){
		currentMenu = inMenu ? MENU_OPTIONS : MENU_INVENTORY;
		optCursor = 0;
		
		cursor2.setVisible(inMenu);
		cursorDarken(inMenu);
		itemDescription.setVisible(!inMenu);
		
		if(inMenu){
			for(Icon i:statIcons) i.setVisible(false);
			for(Text t:statTexts) t.setVisible(false);
			
			
			Item i = currentItem();
			itemOptions.clear();
			
			//if(i.isUsable())
			//	itemOptions.add(ITEM_USE);
			
			if(i instanceof EquippableItem){
				EquippableItem e = (EquippableItem)i;
				
				if(((EquippableItem)i).getClassType() == player.getClassType())
					itemOptions.add(ITEM_EQUIP);

				if(e instanceof Weapon)
					itemOptions.add(ITEM_ATTACKS);
				
				if(e.getDef() != 0 || e.getMaxHP() != 0 || e.getMaxMP() != 0 || e.getStatusEffectImmunities() != null)
					itemOptions.add(ITEM_STATS);
			}
			else if(i instanceof ConsumableItem)
				itemOptions.add(ITEM_USE);
			
			itemOptions.add(ITEM_DROP);
			
			
			String op = "";
			
			for(int j = 0; j < itemOptions.size(); j++){
				switch(itemOptions.get(j)){
					case ITEM_USE:		op += "Use";		break;
					case ITEM_EQUIP:	op += invCursor >= 0 ? "Equip" : "Unequip"; break;
					case ITEM_STATS:	op += "Stats";		break;
					case ITEM_ATTACKS:	op += "Attacks";	break;
					case ITEM_DROP:		op += "Drop";		break;
				}
				
				if(j != itemOptions.size() - 1)
					op += "\n";
			}
			
			itemTierOptions.setText(op);
			return;
		}
		
		for(Icon i:statIcons) i.setVisible(false);
		for(Text t:statTexts) t.setVisible(false);
		
		updateItemInfo();
	}
	
	private void itemOption(int option){
		switch(option){
			
			case ITEM_USE:
				useItem();
				return;
			
			case ITEM_EQUIP:
				equipItem();
				return;
			
			case ITEM_STATS:
				itemStats(true);
				return;
			
			case ITEM_ATTACKS:
				itemAttacks(true);
				return;
			
			case ITEM_DROP:
				itemDrop(true);
				return;
		}
	}
	
	private void useItem(){
		
		ConsumableItem item = (ConsumableItem)currentItem();
		
		player.useItem(item);
		player.removeItem(item);
		
		int c = invCursor;
		
		genItemList();
		
		invCursor = c;
		if(invCursor >= inventory.size())
			invCursor = inventory.size() - 1;
		
		updateItemList();
		updateEquipment();
		updateItemOptions(false);
		updateCursor();
		
		updateStats = true;
	}
	
	private void equipItem(){
		
		Item item = currentItem();
		int slot = item instanceof Weapon ? ((Weapon)item).isSpecialWeapon() ? 1 : 0 : 2;
		
		boolean isEquipped = invCursor < 0;
		
		player.addItem(player.unequip(slot));
		
		if(!isEquipped){
			player.removeItem(item);
			player.equip((EquippableItem)item);
		}
		
		genItemList();
		
		if(isEquipped){
			invCursor = inventory.size() - 1;
			
			if(inventory.size() > INV_MENU_SIZE)
				invScroll = invCursor - (INV_MENU_SIZE - 1);
		}
		else{
			invCursor = -3 + slot;
		}
		
		updateItemList();
		updateEquipment();
		updateItemOptions(false);
		updateCursor();
		
		updateStats = true;
	}
	
	private void itemStats(boolean inMenu){
		currentMenu = inMenu ? MENU_STATS : MENU_OPTIONS;
		
		if(inMenu){
			for(Icon ic:statIcons) ic.delete();
			for(Text t:statTexts) t.delete();
			statIcons.clear();
			statTexts.clear();
			
			Item i = currentItem();
			
			if(i instanceof EquippableItem){
				EquippableItem e = (EquippableItem)i;
				
				int def = e.getDef();
				int hp = e.getMaxHP();
				int mp = e.getMaxMP();
				
				itemTierOptions.setText(
					(def != 0 ? (def > 0 ? "+" : "") + def + " Defense\n" : "") +
					(hp != 0 ? (hp > 0 ? "+" : "") + hp + " Max Health\n" : "") +
					(mp != 0 ? (mp > 0 ? "+" : "") + mp + " Max Mana\n" : "")
				);
				
				int y = 278;
				
				if(def > 0){
					statIcons.add(new Icon(ICON_SHIELD, 46, y, new Color[]{Color.LIGHT_GRAY, Color.DARK_GRAY}, tc));
					y += 22;
				}
				if(hp > 0){
					statIcons.add(new Icon(ICON_HEART, 46, y, tc));
					y += 22;
				}
				if(mp > 0){
					statIcons.add(new Icon(ICON_MANA, 46, y, tc));
					y += 22;
				}
				
				int[] sei = e.getStatusEffectImmunities();
				
				if(sei != null){
					statTexts.add(new Text(72, y, "Status Effect Immunities", -1, Color.BLACK, 1, tc));
					
					for(int j = 0; j < sei.length; j++)
						statIcons.add(new Icon(StatusEffect.getIconType(sei[j]), 72 + j*20, y + 22, StatusEffect.getIconColors(sei[j]), tc));
					
					y += 48;
				}
				
				statTexts.add(new Text(72, y, "Back", -1, Color.BLACK, 1, tc));
				cursor2.setY(y);
				
				icons.addAll(statIcons);
				texts.addAll(statTexts);
			}
		}
		else{
			updateItemOptions(true);
			optCursor = itemOptions.indexOf(ITEM_STATS);
			updateCursor();
		}
	}
	
	private void itemAttacks(boolean inMenu){
		currentMenu = inMenu ? MENU_ATTACKS : MENU_OPTIONS;
		
		if(inMenu){
			itemIcon.setVisible(false);
			
			extraArrows.add(new Icon(ICON_CURSOR_U, 168, 242, tc));
			extraArrows.add(new Icon(ICON_CURSOR_D, 168, 424, tc));
			extraArrows.get(0).setVisible(false);
			icons.addAll(extraArrows);
			
			updateAttackInfo();
		}
		else{
			updateItemOptions(true);
			
			for(Icon i:extraArrows){
				i.setVisible(false);
				i.delete();
			}
			
			itemIcon.setVisible(true);
			itemName.setText(currentItem().getName());
			
			extraArrows.clear();
			optCursor = itemOptions.indexOf(ITEM_ATTACKS);
			
			updateCursor();
		}
	}
	
	private void updateAttackInfo(){
		for(Icon ic:statIcons) ic.delete();
		for(Text t:statTexts) t.delete();
		statIcons.clear();
		statTexts.clear();
		
		itemName.setText((attCursor + 1) + "/" + ((Weapon)currentItem()).getAttacks().length);
		
		Attack a = ((Weapon)currentItem()).getAttacks()[attCursor];
		
		itemTierOptions.setText(a.getName() + "\n" + a.getDescription());
		
		int y = 278 + itemTierOptions.getLines()*22 + 8;
		
		int min = a.getMinDamage();
		int max = a.getMaxDamage();

		statIcons.add(new Icon(ICON_SWORD, 46, y, new Color(128, 128, 128), tc));
		statTexts.add(new Text(66, y, "Damage:" + min + (min == max ? "" : "-" + max) + "   Rate:" + a.getHitRate() + "%", -1, Color.BLACK, 1, tc));
		y += 22;
		
		int mp = a.getManaCost();
		
		if(mp != 0){
			statIcons.add(new Icon(ICON_MANA, 46, y, tc));
			statTexts.add(new Text(66, y, "Cost:" + mp, -1, Color.BLACK, 1, tc));
			y += 22;
		}
		
		StatusEffect[] st = a.getStatusEffects();
		
		if(st != null){
			for(int i = 0; i < st.length; i++){
				StatusEffect s = st[i];
				statIcons.add(new Icon(s.getIconType(), 66 + 20*i, y, s.getIconColors(), tc));
			}
			
			y += 22;
		}
		
		statTexts.add(new Text(66, 418, "Back", -1, Color.BLACK, 1, tc));
		cursor2.setY(418);
		
		icons.addAll(statIcons);
		texts.addAll(statTexts);
	}
	
	private void itemDrop(boolean inMenu){
		currentMenu = inMenu ? MENU_DROP : MENU_OPTIONS;
		
		cursor2.setVisible(inMenu);
		
		if(inMenu){
			itemIcon.setVisible(false);
			
			itemTotalCount = player.getItemCount(currentItem());
			
			if(invCursor < 0)
				itemTotalCount = 1;
			
			if(itemTotalCount == 1){
				dropCursor = 0;
				
				itemName.setText("Are you sure?");
				itemTierOptions.setText("Yes\nNo");
			}
			else{
				dropCursor = 1;
				
				itemName.setText("How many?");
				itemTierOptions.setText("\n1");
				
				extraArrows.add(new Icon(ICON_CURSOR_U, 66, 278, tc));
				extraArrows.add(new Icon(ICON_CURSOR_D, 66, 322, tc));
				extraArrows.get(1).setVisible(false);
				icons.addAll(extraArrows);
				
				cursor2.setVisible(false);
			}
			
			updateCursor();
			return;
		}
		
		for(Icon i:extraArrows){
			i.setVisible(false);
			i.delete();
		}
		
		extraArrows.clear();
		
		itemIcon.setVisible(true);
		updateItemInfo();
		updateItemOptions(true);
		
		optCursor = itemOptions.indexOf(ITEM_DROP);
		updateCursor();
	}
	
	private void cursorDarken(boolean darken){
		cursor.setColors(new Color[]{darken ? new Color(128, 128, 128) : Color.WHITE});
	}
	
	private Item currentItem(){
		
		Item i = invCursor >= 0 ? inventory.isEmpty() ? null : inventory.get(invCursor) : null;
		
		if(i != null)
			return i;
		
		switch(invCursor){
			case -1: i = player.getArmor();			break;
			case -2: i = player.getSpecialWeapon();	break;
			case -3: i = player.getWeapon();		break;
		}
		
		return i;
	}
	
	public boolean updateStats(){
		
		if(updateStats){
			updateStats = false;
			return true;
		}
		
		return false;
	}
}
