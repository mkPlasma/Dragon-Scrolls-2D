package engine.ui;

import engine.Color;
import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public class TextChar extends UISpriteElem{
	
	private float[] color;
	private float scale;
	
	public TextChar(float x, float y, char c, Color color, float scale, TextureCache tc){
		super(x, y, tc);
		
		this.color = new float[]{
			color.getRed()/255f,
			color.getGreen()/255f,
			color.getBlue()/255f
		};
		
		this.scale = scale;
		
		genSprite(c);
	}
	
	private void genSprite(char c){
		
		if(c == 'g' || c == 'j' || c == 'p' || c == 'q' || c == 'y')
			y += 6*scale;
		
		c -= 32;
		
		sprite = new Sprite("ui/fonts/01.png", 8*(c%16), 16*(c/16), 8, 16);
		sprite.setScale(scale);
		tc.loadSprite(sprite);
	}
	
	public float[] getColor(){
		return color;
	}
}
