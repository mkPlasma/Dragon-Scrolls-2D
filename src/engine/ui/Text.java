package engine.ui;

import java.util.ArrayList;

import engine.Color;
import engine.graphics.TextureCache;

public class Text extends UIElement{
	
	private String text;
	
	private int wrap;
	
	private Color color;
	
	private float scale;
	
	private ArrayList<TextChar> chars;
	private int lines;
	
	public Text(float x, float y, String text, int wrap, Color color, float scale, TextureCache tc){
		super(x, y, tc);
		
		this.x = x;
		this.y = y;
		this.text = text;
		this.wrap = wrap;
		this.color = color;
		this.scale = scale;
		
		chars = new ArrayList<TextChar>(text.length());
		genChars();
	}
	
	private void genChars(){
		
		chars.clear();
		
		// Find where to wrap text
		ArrayList<Integer> wrapPoints = new ArrayList<Integer>();
		
		int xl = 0;
		int lastWord = 0;
		
		if(wrap >= 0){
			for(int i = 0; i < text.length(); i++){
				
				char c = text.charAt(i);
				
				// Reset on line break
				if(c == '\n'){
					xl = 0;
					continue;
				}
				
				// Wrap at last space
				else if(c == ' '){
					lastWord = i + 1;
				}
				
				xl++;
				
				// Set wrap point
				if(xl > wrap && !wrapPoints.contains(lastWord) && lastWord > 0){
					wrapPoints.add(lastWord);
					xl = 0;
				}
			}
		}
		
		int line = 0;
		xl = 0;
		
		for(int i = 0; i < text.length(); i++){
			
			char c = text.charAt(i);
			
			if(c == ' '){
				xl++;
				continue;
			}
			
			// Newline/wrap point
			if(c == '\n' || wrapPoints.contains(i)){
				xl = 0;
				line++;
				
				if(c == '\n')
					continue;
			}
			
			// Create text object
			TextChar t = new TextChar(x + xl*(int)(9*scale) + (int)(8*scale), y + line*(int)(22*scale) + (int)(8*scale), c, color, scale, tc);
			chars.add(t);
			xl++;
		}
		
		lines = line + 1;
	}
	
	public void delete(){
		chars.clear();
	}
	
	public void setX(int x){
		float xDif = x - this.x;
		this.x = x;
		
		for(TextChar c:chars)
			c.setX(c.getX() + xDif);
	}
	
	public void setY(int y){
		float yDif = y - this.y;
		this.y = y;
		
		for(TextChar c:chars)
			c.setY(c.getY() + yDif);
	}
	
	public void setText(String text){
		this.text = text;
		genChars();
	}
	
	public String getText(){
		return text;
	}
	
	public ArrayList<TextChar> getChars(){
		return chars;
	}
	
	public int getLines(){
		return lines;
	}
}
