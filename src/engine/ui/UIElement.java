package engine.ui;

import engine.graphics.TextureCache;

public abstract class UIElement{
	
	protected float x, y;
	
	protected boolean deleted;
	protected boolean visible;
	
	protected final TextureCache tc;
	
	public UIElement(float x, float y, TextureCache tc){
		this.x = x;
		this.y = y;
		this.tc = tc;
		
		visible = true;
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public float getX(){
		return x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public float getY(){
		return y;
	}
	
	public void delete(){
		deleted = true;
	}
	
	public boolean isDeleted(){
		return deleted;
	}
	
	public void setVisible(boolean visible){
		this.visible = visible;
	}
	
	public boolean isVisible(){
		return visible;
	}
}
