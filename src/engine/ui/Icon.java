package engine.ui;

import engine.Color;
import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public class Icon extends UISpriteElem{
	
	public static final int
		ICON_CURSOR_R		= 0,
		ICON_CURSOR_L		= 1,
		ICON_CURSOR_D		= 2,
		ICON_CURSOR_U		= 3,
		ICON_BAR_EMPTY		= 4,
		ICON_BAR			= 5,
		ICON_SCROLL_BAR		= 6,
		
		ICON_KNIGHT			= 8,
		ICON_ARCHER			= 9,
		ICON_MAGE			= 10,
		ICON_HEART			= 11,
		ICON_MANA			= 12,
		ICON_XP				= 13,
		ICON_GOLD			= 14,
		
		ICON_SWORD			= 16,
		ICON_BOW			= 17,
		ICON_WAND			= 18,
		ICON_SHIELD			= 19,
		ICON_QUIVER			= 20,
		ICON_SPELL			= 21,
		
		
		ICON_KNIGHT_ARMOR	= 24,
		ICON_ARCHER_ARMOR	= 25,
		ICON_ROBE			= 26,
		ICON_BOTTLE_S		= 27,
		ICON_BOTTLE_M		= 28,
		ICON_BOTTLE_L		= 29,
		ICON_BOTTLE_XL		= 30,

		ICON_DOUBLE_HEART	= 32,
		ICON_DOUBLE_MANA	= 33,
		ICON_HEART_PLUS		= 34,
		ICON_MANA_PLUS		= 35,
		ICON_SHIELD_PLUS	= 36,
		ICON_FIST			= 37,
		ICON_ARROW_SHIELD	= 38,
		ICON_HEART_SHIELD	= 39,
		ICON_DROPLET		= 40,
		ICON_FLAME			= 41,
		ICON_HAND_MANA		= 42,
		ICON_HEART_MINUS	= 43,
		ICON_MANA_MINUS		= 44,
		ICON_SHIELD_MINUS	= 45,
		ICON_FIST_MINUS		= 46,
		ICON_SPIRAL			= 47;
	
	
	private int type;
	
	private float[][] colors;
	
	private float scaleX, scaleY;
	private boolean halfWidth, halfHeight, offset;
	
	public Icon(int type, float x, float y, TextureCache tc){
		super(x + 8, y + 8, tc);
		this.type = type;
		
		scaleX = 1;
		scaleY = 1;
		
		colors = new float[0][];
		genSprite();
	}
	
	public Icon(int type, float x, float y, Color color, TextureCache tc){
		super(x + 8, y + 8, tc);
		this.type = type;
		
		scaleX = 1;
		scaleY = 1;
		
		setColors(new Color[]{color});
		genSprite();
	}
	
	public Icon(int type, float x, float y, Color[] colors, TextureCache tc){
		super(x + 8, y + 8, tc);
		this.type = type;
		
		scaleX = 1;
		scaleY = 1;
		
		setColors(colors);
		genSprite();
	}
	
	// For bars
	public Icon(int type, float x, float y, float scaleX, boolean halfWidth, TextureCache tc){
		super(x + 8, y + 8, tc);
		this.type = type;
		this.scaleX = scaleX;
		this.halfWidth = halfWidth;
		offset = halfWidth;
		
		scaleY = 1;
		
		colors = new float[0][];
		genSprite();
	}
	
	public Icon(int type, float x, float y, float scaleX, boolean halfWidth, Color color, TextureCache tc){
		super(x + 8, y + 8, tc);
		this.type = type;
		this.type = type;
		this.scaleX = scaleX;
		this.halfWidth = halfWidth;
		offset = halfWidth;
		
		scaleY = 1;
		
		setColors(new Color[]{color});
		genSprite();
	}
	
	// For scroll bars
	public Icon(int type, float x, float y, float scaleY, boolean halfWidth, boolean halfHeight, boolean offset, TextureCache tc){
		super(x + 4, y + 8, tc);
		this.type = type;
		this.scaleY = scaleY;
		this.halfWidth = halfWidth;
		this.halfHeight = halfHeight;
		this.offset = offset;
		
		scaleX = 1;
		
		colors = new float[0][];
		genSprite();
	}
	
	
	public void setColors(Color[] col){
		
		colors = new float[col.length][3];
		
		for(int i = 0; i < colors.length; i++)
			colors[i] = new float[]{col[i].getRed()/255f, col[i].getGreen()/255f, col[i].getBlue()/255f};
	}
	
	private void genSprite(){
		sprite = new Sprite("ui/icons.png", 16*(type%8) + (offset ? 8 : 0), 16*(type/8) + (halfHeight ? 8 : 0), halfWidth ? 8 : 16, halfHeight ? 8 : 16);
		sprite.setScaleX(scaleX);
		sprite.setScaleY(scaleY);
		tc.loadSprite(sprite);
	}
	
	public void setX(float x){
		this.x = x + 8;
	}
	
	public void setY(float y){
		this.y = y + 8;
	}
	
	public void setType(int type){
		this.type = type;
		genSprite();
	}
	
	public float[][] getColors(){
		return colors;
	}
}
