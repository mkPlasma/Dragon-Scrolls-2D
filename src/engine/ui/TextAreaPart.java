package engine.ui;

import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public class TextAreaPart extends UISpriteElem{
	
	public static final int
		SCROLL_TOP_LEFT		= 0,
		SCROLL_TOP_MIDDLE	= 1,
		SCROLL_TOP_RIGHT	= 2,
		SCROLL_LEFT			= 3,
		SCROLL_MIDDLE		= 4,
		SCROLL_RIGHT		= 5;
	
	public TextAreaPart(int type, float x, float y, int width, int height, int rotation, float scaleX, float scaleY, TextureCache tc){
		super(x, y, tc);
		initSprite(type, width, height, rotation, scaleX, scaleY);
	}
	
	private void initSprite(int type, int width, int height, int rotation, float scaleX, float scaleY){
		int tx = 0,
			ty = 0;
		
		switch(type){
			case SCROLL_TOP_LEFT:
				tx = 0;
				ty = 0;
				break;
			case SCROLL_TOP_MIDDLE:
				tx = 32;
				ty = 0;
				break;
			case SCROLL_TOP_RIGHT:
				tx = 224;
				ty = 0;
				break;
			case SCROLL_LEFT:
				tx = 0;
				ty = 32;
				break;
			case SCROLL_MIDDLE:
				tx = 32;
				ty = 32;
				break;
			case SCROLL_RIGHT:
				tx = 192;
				ty = 32;
				break;
		}
		
		sprite = new Sprite("ui/text_area.png", tx, ty, width, height);
		sprite.setScaleX(scaleX);
		sprite.setScaleY(scaleY);
		sprite.setRotation(rotation);
		tc.loadSprite(sprite);
	}
}
