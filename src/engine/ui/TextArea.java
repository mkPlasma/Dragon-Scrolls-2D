package engine.ui;

import static engine.ui.TextAreaPart.*;

import java.util.ArrayList;

import engine.graphics.TextureCache;

public class TextArea extends UIElement{
	
	private static final int OPEN_SPEED = 6;
	
	private int width, height;
	
	// Final properties after spawn animation
	private final float fy;
	private final int fHeight;
	
	private final boolean horizontal;
	private final float scale;
	
	private ArrayList<TextAreaPart> parts;
	
	private boolean open;
	private boolean opening;
	private boolean closing;
	
	public TextArea(float x, float y, int width, int height, boolean horizontal, float scale, TextureCache tc){
		super(x, y, tc);
		
		this.horizontal = horizontal;
		this.scale = scale;
		
		// Set for opening animation
		this.height = 0;
		
		if(!horizontal){
			this.x = x;
			this.y = y + height/2f;
			this.width = width;
			
			fy = y;
			fHeight = height;
		}
		
		// Flip if horizontal
		else{
			this.x = y;
			this.y = x;
			this.width = height;
			
			fy = x;
			fHeight = width;
		}
		
		open();
		
		parts = new ArrayList<TextAreaPart>();
		genParts();
	}
	
	public void open(){
		opening = true;
	}
	
	private void genParts(){
		
		parts.clear();
		
		// Top left/Right top
		float x2 = x;
		float y2 = y;
		int w = (int)Math.min(width/scale, 224);
		int h = 32;
		
		addPart(SCROLL_TOP_LEFT, x2, y2, w, h, horizontal);
		
		// Top middle/Right middle
		for(int i = 0; i < Math.ceil((width - 224*scale)/(192*scale)); i++){
			x2 = x + 224*scale + i*(192*scale);
			y2 = y;
			w = (int)Math.min(width/scale - 224 - i*192, 192);
			h = 32;
			
			addPart(SCROLL_TOP_MIDDLE, x2, y2, w, h, horizontal);
		}
		
		// Top right/Right bottom
		x2 = x + width;
		y2 = y;
		w = 32;
		h = 32;
		addPart(SCROLL_TOP_RIGHT, x2, y2, w, h, horizontal);
		
		// Body
		for(int i = 0; i < Math.ceil(height/(224*scale)); i++){
			
			// Left
			x2 = x;
			y2 = y + 32*scale + i*(224*scale);
			w = (int)Math.min(width/scale - 32*scale, 192);
			h = (int)Math.min(height/scale - i*224, 224);
			addPart(SCROLL_LEFT, x2, y2, w, h, horizontal);
			
			// Middle
			for(int j = 0; j < Math.ceil((width - 224*scale)/(160*scale)); j++){
				x2 = x + 192*scale + j*(160*scale);
				y2 = y + 32*scale + i*(224*scale);
				w = (int)Math.min(width/scale - 224 - j*160, 160);
				h = (int)Math.min(height/scale - i*224, 224);
				addPart(SCROLL_MIDDLE, x2, y2, w, h, horizontal);
			}
			
			// Right
			x2 = x - 32*scale + width;
			y2 = y + 32*scale + i*(224*scale);
			w = 32;
			h = (int)Math.min(height/scale - i*224, 224);
			addPart(SCROLL_RIGHT, x2, y2, w, h, horizontal);
		}
		
		// Bottom left
		x2 = x;
		y2 = y + 32*scale + height;
		w = (int)Math.min(width/scale, 224);
		h = 32;
		addPart(SCROLL_TOP_LEFT, x2, y2, w, h, !horizontal);
		
		// Bottom middle
		for(int i = 0; i < Math.ceil((width - 224*scale)/(192*scale)); i++){
			x2 = x + 224*scale + i*(192*scale);
			y2 = y + 32*scale + height;
			w = (int)Math.min(width/scale - 224 - i*192, 192);
			h = 32;
			addPart(SCROLL_TOP_MIDDLE, x2, y2, w, h, !horizontal);
		}
		
		// Bottom right
		x2 = x + width;
		y2 = y + 32*scale + height;
		w = 32;
		h = 32;
		addPart(SCROLL_TOP_RIGHT, x2, y2, w, h, !horizontal);
	}
	
	private void addPart(int type, float x2, float y2, int w, int h, boolean flip){
		x2 += (w*scale)/2;
		y2 += (h*scale)/2;
		
		if(horizontal){
			float t = x2;
			x2 = y2;
			y2 = t;
		}
		
		parts.add(new TextAreaPart(type, x2, y2, w, h, horizontal ? 90 : 0, scale, flip ? -scale : scale, tc));
	}
	
	public void update(){
		
		if(closing){
			height -= height/OPEN_SPEED + 2;
			
			if(height <= 0){
				height = 0;
				closing = false;
				open = false;
				return;
			}
			
			y = fy + fHeight/2f - height/2f;
			
			genParts();
		}
		if(opening){
			height += (fHeight - height)/OPEN_SPEED + 1;
			
			if(height >= fHeight){
				height = fHeight;
				opening = false;
				open = true;
			}
			
			y = fy + fHeight/2f - height/2f;
			
			genParts();
		}
	}
	
	public void close(){
		closing = true;
	}
	
	public boolean isOpen(){
		return open;
	}
	
	public ArrayList<TextAreaPart> getParts(){
		return parts;
	}
}
