package engine.objects;

public class Map{
	
	// Map tiles
	private Tile[][][] tiles;
	
	// Size of map
	private int width, height;
	
	
	public Map(Tile[][][] tiles){
		this.tiles = tiles;
		
		width = tiles.length;
		
		// Get height
		for(Tile[][] t:tiles)
			height = Math.max(height, t.length);
	}
	
	public void setTile(int x, int y, int z, Tile tile){
		tiles[x][y][z] = tile;
	}
	
	public Tile[][][] getTiles(){
		return tiles;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getDiagonalLength(boolean d){
		
		int l = 0;
		
		if(!d){
			for(int i = 0; i < tiles.length; i++)
				if(tiles[i].length >= i)
					l++;
		}
		else
			for(int i = tiles.length - 1; i >= 0; i--)
				if(tiles[i].length >= l + 1)
					l++;
		
		return l;
	}
}
