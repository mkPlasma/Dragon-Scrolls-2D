package engine.objects;

import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public abstract class GameObject{
	
	protected boolean visible;
	
	protected Sprite sprite;
	
	protected TextureCache tc;
	
	public GameObject(TextureCache tc){
		this.tc = tc;
		visible = true;
	}
	
	public GameObject(Sprite sprite, TextureCache tc){
		this.sprite = sprite;
		this.tc = tc;
		visible = true;
	}
	
	public void setVisible(boolean visible){
		this.visible = visible;
	}
	
	public boolean isVisible(){
		return visible;
	}
	
	public void setSprite(Sprite sprite){
		this.sprite = sprite;
	}
	
	public Sprite getSprite(){
		return sprite;
	}
}
