package engine.objects.items;

import engine.Color;

public class ConsumableItem extends Item{
	
	private final int hp, mp;
	private final StatusEffect[] statusEffects;
	
	public ConsumableItem(String name, String description, int tier, int hp, int mp, int iconType, Color[] colors){
		this(name, description, tier, 5, hp, mp, null, iconType, colors);
	}
	
	public ConsumableItem(String name, String description, int tier, StatusEffect[] statusEffects, int iconType, Color[] colors){
		this(name, description, tier, 5, 0, 0, statusEffects, iconType, colors);
	}
	
	public ConsumableItem(String name, String description, int tier, int hp, int mp, StatusEffect[] statusEffects, int iconType, Color[] colors){
		this(name, description, tier, 5, hp, mp, statusEffects, iconType, colors);
	}
	
	public ConsumableItem(String name, String description, int tier, int stackSize, int hp, int mp, StatusEffect[] statusEffects, int iconType, Color[] colors){
		super(name, description, tier, stackSize, iconType, colors);
		this.hp = hp;
		this.mp = mp;
		this.statusEffects = statusEffects;
		
	}
	
	public int getHP(){
		return hp;
	}
	
	public int getMP(){
		return mp;
	}
	
	public StatusEffect[] getStatusEffects(){
		return statusEffects;
	}
}
