package engine.objects.items;

import engine.Color;
import engine.ui.Icon;

public class Weapon extends EquippableItem{
	
	private final Attack[] attacks;
	private final boolean specialWeapon;
	
	public Weapon(String name, String description, int classType, int tier,  boolean specialWeapon, Color color, Attack[] attacks){
		this(name, description, classType, tier, specialWeapon, new Color[]{color}, attacks);
	}

	public Weapon(String name, String description, int classType, int tier, boolean specialWeapon, Color[] colors, Attack[] attacks){
		super(name, description, classType, tier, specialWeapon ? Icon.ICON_SHIELD : Icon.ICON_SWORD, colors);
		this.attacks = attacks;
		this.specialWeapon = specialWeapon;
	}
	
	public Attack[] getAttacks(){
		return attacks;
	}
	
	public boolean isSpecialWeapon(){
		return specialWeapon;
	}
}
