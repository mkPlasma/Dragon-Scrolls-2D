package engine.objects.items;

import engine.Color;
import static engine.ui.Icon.*;

public class StatusEffect{
	
	public static final int
		TYPE_HP_REGEN		= 0,
		TYPE_MP_REGEN		= 1,
		TYPE_HP_BUFF		= 2,
		TYPE_MP_BUFF		= 3,
		TYPE_DEF_BUFF		= 4,
		TYPE_DMG_BUFF		= 5,
		TYPE_DMG_BLOCK		= 6,
		TYPE_DMG_TO_HP		= 7,
		
		TYPE_HP_DRAIN_BLEED		= 8,
		TYPE_HP_DRAIN_BURN		= 9,
		TYPE_MP_DRAIN_POISON	= 10,
		TYPE_MP_DRAIN_STEAL		= 11,
		TYPE_HP_DEBUFF			= 12,
		TYPE_MP_DEBUFF			= 13,
		TYPE_DEF_DEBUFF			= 14,
		TYPE_DMG_DEBUFF			= 15,
		TYPE_DMG_FAIL			= 16;
	
	private final int type, level, chance;
	private int duration;
	
	// For attacks/items with status effects on self
	private final boolean onSelf;
	
	public StatusEffect(int type, int level, int duration, int chance, boolean self){
		this.type = type;
		this.level = level;
		this.duration = duration;
		this.chance = chance;
		this.onSelf = self;
	}
	
	public int getIconType(){
		return getIconType(type);
	}
	
	public static int getIconType(int type){
		switch(type){
			case TYPE_HP_REGEN:		return ICON_DOUBLE_HEART;
			case TYPE_MP_REGEN:		return ICON_DOUBLE_MANA;
			case TYPE_HP_BUFF:		return ICON_HEART_PLUS;
			case TYPE_MP_BUFF:		return ICON_MANA_PLUS;
			case TYPE_DEF_BUFF:		return ICON_SHIELD_PLUS;
			case TYPE_DMG_BUFF:		return ICON_FIST;
			case TYPE_DMG_BLOCK:	return ICON_ARROW_SHIELD;
			case TYPE_DMG_TO_HP:	return ICON_HEART_SHIELD;
			
			case TYPE_HP_DRAIN_BLEED:	return ICON_DROPLET;
			case TYPE_HP_DRAIN_BURN:	return ICON_FLAME;
			case TYPE_MP_DRAIN_POISON:	return ICON_DROPLET;
			case TYPE_MP_DRAIN_STEAL:	return ICON_HAND_MANA;
			case TYPE_HP_DEBUFF:		return ICON_HEART_MINUS;
			case TYPE_MP_DEBUFF:		return ICON_MANA_MINUS;
			case TYPE_DEF_DEBUFF:		return ICON_SHIELD_MINUS;
			case TYPE_DMG_DEBUFF:		return ICON_FIST_MINUS;
			case TYPE_DMG_FAIL:			return ICON_SPIRAL;
		}
		
		return 0;
	}
	
	public Color[] getIconColors(){
		return getIconColors(type);
	}
	
	public static Color[] getIconColors(int type){
		switch(type){
			case TYPE_HP_DRAIN_BLEED:
				return new Color[]{Color.RED, Color.WHITE};
			
			case TYPE_MP_DRAIN_POISON:
				return new Color[]{new Color(0, 128, 0), Color.WHITE};
		}
		
		return new Color[]{Color.BLACK};
	}
	
	public static String getName(int type){
		switch(type){
			case TYPE_HP_REGEN:		return "Health regen";
			case TYPE_MP_REGEN:		return "Mana regen";
			case TYPE_HP_BUFF:		return "Health increase";
			case TYPE_MP_BUFF:		return "Magic increase";
			case TYPE_DEF_BUFF:		return "Defense increase";
			case TYPE_DMG_BUFF:		return "Strength";
			case TYPE_DMG_BLOCK:	return "Barrier";
			case TYPE_DMG_TO_HP:	return "Absorption";
			
			case TYPE_HP_DRAIN_BLEED:	return "Bleed";
			case TYPE_HP_DRAIN_BURN:	return "Burn";
			case TYPE_MP_DRAIN_POISON:	return "Poison";
			case TYPE_MP_DRAIN_STEAL:	return "Magic steal";
			case TYPE_HP_DEBUFF:		return "Health decrease";
			case TYPE_MP_DEBUFF:		return "Magic decrease";
			case TYPE_DEF_DEBUFF:		return "Defense break";
			case TYPE_DMG_DEBUFF:		return "Weakness";
			case TYPE_DMG_FAIL:			return "Confusion";
		}
		
		return "";
	}
	
	public int getType(){
		return type;
	}
	
	public int getLevel(){
		return level;
	}
	
	public int getChance(){
		return chance;
	}
	
	public boolean tickDuration(){
		duration--;
		return duration == 0;
	}
	
	public int getDuration(){
		return duration;
	}
	
	public boolean onSelf(){
		return onSelf;
	}
}
