package engine.objects.items;

import engine.Color;

public abstract class Item{
	
	private final String name;
	private final String description;
	
	private final int tier;
	
	private final int stackSize;
	
	private final Color[] colors;
	
	protected final int iconType;

	public Item(String name, String description, int tier, int stackSize, int iconType, Color color){
		this(name, description, tier, stackSize, iconType, new Color[]{color});
	}
	
	public Item(String name, String description, int tier, int stackSize, int iconType, Color[] colors){
		this.name = name;
		this.description = description;
		this.tier = tier;
		this.stackSize = stackSize;
		this.iconType = iconType;
		this.colors = colors;
	}
	
	public boolean equals(Item item){
		return name == item.getName();
	}
	
	/*public boolean equals(Item item){
		
		Color[] colors2 = item.getColors();
		boolean colorsEqual = colors.length == colors2.length;
		
		if(colorsEqual){
			for(int i = 0; i < colors.length; i++){
				if(!colors[i].equals(colors2[i])){
					colorsEqual = false;
					break;
				}
			}
		}
		
		return
			colorsEqual &&
			name.equals(item.getName()) &&
			description.equals(item.getDescription()) &&
			tier		== item.getTier() &&
			usable		== item.isUsable() &&
			equippable	== item.isEquippable() &&
			stackSize	== item.getStackSize() && 
			iconType	== item.getIconType();
	}*/
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public int getTier(){
		return tier;
	}
	
	public int getStackSize(){
		return stackSize;
	}
	
	public Color[] getColors(){
		return colors;
	}
	
	public int getIconType(){
		return iconType;
	}
}
