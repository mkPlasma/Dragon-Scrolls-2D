package engine.objects.items;

public class Attack{
	
	public static final int
		ROLE_DAMAGE		= 0;
	
	private final String name, description;
	private final int minDamage, maxDamage, hitRate;
	
	private final int manaCost;
	
	private StatusEffect[] statusEffects;
	
	// AI attack role
	// Determines when attack is used
	private final int aiRole;
	
	
	// Player standard
	public Attack(String name, String description, int minDamage, int maxDamage, int hitRate){
		this(name, description, minDamage, maxDamage, null, hitRate, 0, 0);
	}
	
	public Attack(String name, String description, int minDamage, int maxDamage, StatusEffect[] statusEffects, int hitRate){
		this(name, description, minDamage, maxDamage, statusEffects, hitRate, 0, 0);
	}
	
	// Player special
	public Attack(String name, String description, int minDamage, int maxDamage, int hitRate, int manaCost){
		this(name, description, minDamage, maxDamage, null, hitRate, manaCost, 0);
	}
	
	public Attack(String name, String description, int minDamage, int maxDamage, StatusEffect[] statusEffects, int hitRate, int manaCost){
		this(name, description, minDamage, maxDamage, statusEffects, hitRate, manaCost, 0);
	}
	
	// Enemy
	public Attack(int aiRole, String name, String description, int minDamage, int maxDamage, int hitRate){
		this(name, description, minDamage, maxDamage, null, hitRate, 0, aiRole);
	}
	
	public Attack(int aiRole, String name, String description, int minDamage, int maxDamage, StatusEffect[] statusEffects, int hitRate){
		this(name, description, minDamage, maxDamage, statusEffects, hitRate, 0, aiRole);
	}
	
	public Attack(String name, String description, int minDamage, int maxDamage, StatusEffect[] statusEffects, int hitRate, int manaCost, int aiRole){
		this.name = name;
		this.description = description;
		this.minDamage = minDamage;
		this.maxDamage = maxDamage;
		this.statusEffects = statusEffects;
		this.hitRate = hitRate;
		this.manaCost = manaCost;
		this.aiRole = aiRole;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public int getMinDamage(){
		return minDamage;
	}
	
	public int getMaxDamage(){
		return maxDamage;
	}
	
	public int getHitRate(){
		return hitRate;
	}
	
	public int getManaCost(){
		return manaCost;
	}
	
	public StatusEffect[] getStatusEffects(){
		return statusEffects;
	}
	
	public int getAIRole(){
		return aiRole;
	}
}
