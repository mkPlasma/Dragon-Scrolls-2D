package engine.objects.items;

import engine.Color;
import engine.ui.Icon;

public class EquippableItem extends Item{
	
	protected int classType;
	protected int def, maxHp, maxMp;
	protected int[] statusEffectImmunities;
	
	protected Color[] playerColors;
	
	// For weapons
	public EquippableItem(String name, String description, int classType, int tier, int iconType, Color[] colors){
		this(name, description, classType, tier, 0, 0, 0, null, iconType, colors, colors);
	}
	
	// For armors
	public EquippableItem(String name, String description, int classType, int tier, int def, int maxHp, int maxMp, Color color, Color[] playerColors){
		this(name, description, classType, tier, def, maxHp, maxMp, null, Icon.ICON_KNIGHT_ARMOR, new Color[]{color}, playerColors);
	}
	
	public EquippableItem(String name, String description, int classType, int tier, int def, int maxHp, int maxMp, Color[] colors, Color[] playerColors){
		this(name, description, classType, tier, def, maxHp, maxMp, null, Icon.ICON_KNIGHT_ARMOR, colors, playerColors);
	}
	
	public EquippableItem(String name, String description, int classType, int tier, int def, Color color, Color[] playerColors){
		this(name, description, classType, tier, def, 0, 0, null, Icon.ICON_KNIGHT_ARMOR, new Color[]{color}, playerColors);
	}
	
	public EquippableItem(String name, String description, int classType, int tier, int def, Color[] colors, Color[] playerColors){
		this(name, description, classType, tier, def, 0, 0, null, Icon.ICON_KNIGHT_ARMOR, colors, playerColors);
	}
	
	public EquippableItem(String name, String description, int classType, int tier, int def, int maxHp, int maxMp,
		int[] statusEffectImmunities, int iconType, Color[] colors, Color[] playerColors){
		super(name, description, tier, 1, iconType + classType, colors);
		
		this.classType = classType;
		this.def = def;
		this.maxHp = maxHp;
		this.maxMp = maxMp;
		this.statusEffectImmunities = statusEffectImmunities;
		this.playerColors = playerColors;
	}
	
	/*public boolean equals(Item item){
		
		if(!(item instanceof EquippableItem))
			return false;
		
		EquippableItem it = (EquippableItem)item;
		
		int[] se = it.getStatusEffectImmunities();
		boolean statusImmunitiesEqual = (statusEffectImmunities == null && se == null) || (statusEffectImmunities != null && se != null);
		
		if(se != null && statusImmunitiesEqual){
			statusImmunitiesEqual = statusEffectImmunities.length == se.length;
			
			if(statusImmunitiesEqual){
				for(int i = 0; i < statusEffectImmunities.length; i++){
					if(statusEffectImmunities[i] != se[i]){
						statusImmunitiesEqual = false;
						break;
					}
				}
			}
		}
		
		return
			statusImmunitiesEqual &&
			super.equals(it) &&
			classType	== it.getClassType() &&
			def			== it.getDef() &&
			maxHp		== it.getMaxHP() &&
			maxMp		== it.getMaxMP();
	}*/
	
	public boolean grantsImmunity(int statusEffect){
		
		if(statusEffectImmunities == null)
			return false;
		
		for(int i:statusEffectImmunities)
			if(i == statusEffect)
				return true;
		
		return false;
	}
	
	public int getClassType(){
		return classType;
	}
	
	public int getDef(){
		return def;
	}
	
	public int getMaxHP(){
		return maxHp;
	}
	
	public int getMaxMP(){
		return maxMp;
	}
	
	public int[] getStatusEffectImmunities(){
		return statusEffectImmunities;
	}
	
	public Color[] getPlayerColors(){
		return playerColors;
	}
}
