package engine.objects;

import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public abstract class GameEntity extends GameObject{
	
	protected float x, y;
	
	protected boolean deleted;
	
	public GameEntity(float x, float y, TextureCache tc){
		super(tc);
		this.x = x;
		this.y = y;
	}
	
	public GameEntity(float x, float y, Sprite sprite, TextureCache tc){
		super(sprite, tc);
		this.x = x;
		this.y = y;
	}
	
	public abstract void update();
	
	public void setX(float x){
		this.x = x;
	}
	
	public float getX(){
		return x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public float getY(){
		return y;
	}
	
	public void delete(){
		deleted = true;
	}
	
	public boolean isDeleted(){
		return deleted;
	}
}
