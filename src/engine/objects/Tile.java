package engine.objects;

import engine.graphics.Sprite;
import engine.graphics.TextureCache;

public class Tile extends GameObject{
	
	public static final int NUM_TILES = 16;
	
	public static final int
		TILE_GRASS		= 0,
		TILE_DIRT		= 1,
		TILE_GRAVEL		= 2,
		TILE_BRICK		= 3;
	
	
	public static final short
		SHADOW_LEFT				= 0b1,
		SHADOW_TOP				= 0b10,
		SHADOW_RIGHT			= 0b100,
		SHADOW_BOTTOM			= 0b1000,
		SHADOW_SIDE_LEFT		= 0b10000,
		SHADOW_SIDE_RIGHT		= 0b100000,
		SHADOW_CORNER_TOP		= 0b1000000,
		SHADOW_CORNER_LEFT		= 0b10000000,
		SHADOW_CORNER_RIGHT		= 0b100000000,
		SHADOW_ROUND			= 0b1000000000,
		SHADOW_FULL_LIGHT		= 0b10000000000,
		SHADOW_FULL_DARK		= 0b100000000000;
	
	
	private int type;
	
	// Which shadows to display
	private short shadow;
	
	// Whether player/NPCs can walk through tile
	private boolean solid;
	
	public Tile(int type, TextureCache tc){
		super(tc);
		this.type = type;
		
		init();
	}
	
	private void init(){
		solid = type < 0;
		
		setSprite();
	}
	
	private void setSprite(){
		
		if(type == -1){
			visible = false;
			type = 0;
		}
		
		int tx = 0;
		int ty = 0;

		tx = (type%4)*32;
		ty = (type/4)*32;
		
		sprite = new Sprite("tiles/01.png", tx, ty, 32, 32);
		
		if(tc != null)
			tc.loadSprite(sprite);
	}
	
	public void setType(int type){
		this.type = type;
	}
	
	public int getType(){
		return type;
	}
	
	public void addShadow(short shadow){
		this.shadow |= shadow;
	}
	
	public boolean hasShadow(short shadow){
		return (this.shadow & shadow) != 0;
	}
	
	public void setShadow(short shadow){
		this.shadow = shadow;
	}
	
	public short getShadow(){
		return shadow;
	}
	
	public void setSolid(boolean solid){
		this.solid = solid;
	}
	
	public boolean isSolid(){
		return solid;
	}
}
