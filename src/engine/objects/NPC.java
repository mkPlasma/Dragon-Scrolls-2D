package engine.objects;

import org.lwjgl.glfw.GLFW;

import engine.KeyboardListener;
import engine.graphics.TextureCache;

public class NPC extends GameEntity{
	
	public static final int
		DIR_RIGHT	= 0,
		DIR_LEFT	= 1,
		DIR_DOWN	= 2,
		DIR_UP		= 3;
	
	private int moveSpeed;
	
	// Directions movement is allowed in
	private boolean[] movementsAllowed = new boolean[4];
	
	// Direction of movement
	private int direction;
	
	// Time of movement
	private int moveTime;
	
	// Current tile
	private int tileX, tileY, tileZ;
	
	// Sprite animation
	private int spriteCycle;
	
	
	public NPC(float x, float y, TextureCache tc){
		super(x, y, tc);
	}
	
	
	public void update(){
		
		// Movement
		if(moveTime > 0){
			switch(direction){
				case 0: x += 1f/moveSpeed;	break;
				case 1: x -= 1f/moveSpeed;	break;
				case 2: y += 1f/moveSpeed;	break;
				case 3: y -= 1f/moveSpeed;	break;
			}
			
			if(moveTime % (moveSpeed/3) == 0)
				spriteCycle++;
			
			moveTime--;
		}
		
		if(moveTime == 0){
			spriteCycle = 0;
			
			if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_RIGHT)){
				direction = DIR_RIGHT;
				
				if(movementsAllowed[0]){
					tileX++;
					moveTime = moveSpeed;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_LEFT)){
				direction = DIR_LEFT;
				
				if(movementsAllowed[1]){
					tileX--;
					moveTime = moveSpeed;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_DOWN)){
				direction = DIR_DOWN;
				
				if(movementsAllowed[2]){
					tileY++;
					moveTime = moveSpeed;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_UP)){
				direction = DIR_UP;
				
				if(movementsAllowed[3]){
					tileY--;
					moveTime = moveSpeed;
				}
			}
		}
		
		updateSprite();
	}
	
	private void updateSprite(){
		
		int x = 0;
		int y = 0;
		
		if(spriteCycle == 0 || spriteCycle == 2)
			x = 0;
		if(spriteCycle == 1)
			x = 32;
		if(spriteCycle == 3)
			x = 64;
		
		if(direction == DIR_LEFT || direction == DIR_UP)
			y += 32;

		if(direction == DIR_RIGHT || direction == DIR_LEFT)
			sprite.setScaleX(1);
		else
			sprite.setScaleX(-1);
		
		sprite.setX(x);
		sprite.setY(y);
		sprite.genTextureCoords();
	}
	
	public int getTileX(){
		return tileX;
	}
	
	public int getTileY(){
		return tileY;
	}
	
	public int getTileZ(){
		return tileZ;
	}
	
}
