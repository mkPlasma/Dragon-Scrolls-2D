package engine.objects;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import content.ItemList;
import engine.KeyboardListener;
import engine.graphics.Sprite;
import engine.graphics.TextureCache;
import engine.objects.items.ConsumableItem;
import engine.objects.items.EquippableItem;
import engine.objects.items.Item;
import engine.objects.items.StatusEffect;
import engine.objects.items.Weapon;

import static engine.objects.NPC.*;
import static engine.objects.items.StatusEffect.*;


public class Player extends GameEntity{
	
	// Gameplay properties
	
	public static final int
		CLASS_KNIGHT	= 0,
		CLASS_ARCHER	= 1,
		CLASS_MAGE		= 2;
	
	private int classType;
	
	private int hp, maxHp, baseMaxHp;
	private int mp, maxMp, baseMaxMp;
	private int def, baseDef;
	private int level, xp, xpReq;
	private int gold;
	
	private ArrayList<StatusEffect> statusEffects;
	
	private Weapon weapon, specialWeapon;
	private EquippableItem armor;
	
	
	private ArrayList<Item> inventory;
	private ArrayList<Integer> inventoryCounts;
	private int inventorySize;
	
	
	// Map screen properties
	
	// Time it takes to move one tile
	private static final int MOVE_TIME = 12;
	
	// Directions movement is allowed in
	private boolean[] movementsAllowed = new boolean[4];
	
	// Direction of movement
	private int direction;
	
	// Time of movement
	private int moveTime;
	
	// Current tile
	private int tileX, tileY, tileZ;
	
	// Sprite animation
	private int spriteCycle;
	
	private Sprite weaponSprite, specialWeaponSprite;
	
	
	public Player(float x, float y, int classType, TextureCache tc){
		super(x, y, tc);
		this.classType = classType;
		
		statusEffects = new ArrayList<StatusEffect>();
		
		levelUp();
		
		inventorySize = 20;
		inventory = new ArrayList<Item>(inventorySize);
		inventoryCounts = new ArrayList<Integer>(inventorySize);
		
		// tmp
		ItemList itemList = new ItemList();

		addItem(itemList.getArmor(CLASS_KNIGHT, 0));
		addItem(itemList.getArmor(CLASS_KNIGHT, 1));
		addItem(itemList.getWeapon(CLASS_KNIGHT, 0));
		
		for(int i = 0; i < 5; i++)
			addItem(itemList.getConsumable(i), 5);
		
		weapon = itemList.getWeapon(CLASS_KNIGHT, 0);
		armor = itemList.getArmor(CLASS_KNIGHT, 1);
		
		updateStats();
		
		gold = 100;
		
		tileX = (int)x;
		tileY = (int)y;
		tileZ = 0;
		
		sprite = new Sprite("player.png", 0, 0, 32, 32);
		tc.loadSprite(sprite);
		
		weaponSprite		= new Sprite("player.png", 0, 0, 32, 32);
		specialWeaponSprite	= new Sprite("player.png", 0, 0, 32, 32);
		tc.loadSprite(weaponSprite);
		tc.loadSprite(specialWeaponSprite);
		
		updateSprite();
	}
	
	// General
	
	private void levelUp(){
		
		level++;
		xp -= xpReq;
		
		// Gain multiplier
		float m = (float)Math.pow(level - 1, 1.2);
		
		xpReq = 50 + (int)(5*m);
		
		switch(classType){
			case CLASS_KNIGHT:
				baseMaxHp = 200 + (int)(11*m);
				baseMaxMp = 100 + (int)(5*m);
				break;
			case CLASS_ARCHER:
				baseMaxHp = 200 + (int)(8*m);
				baseMaxMp = 100 + (int)(8*m);
				break;
			case CLASS_MAGE:
				baseMaxHp = 200 + (int)(5*m);
				baseMaxMp = 100 + (int)(11*m);
				break;
		}
		
		if(xp >= xpReq)
			levelUp();
		
		updateStats();
		restoreStats();
	}
	
	public void updateStats(){
		maxHp = baseMaxHp;
		maxMp = baseMaxMp;
		def = 0;
		
		if(weapon != null){
			maxHp += weapon.getMaxHP();
			maxMp += weapon.getMaxMP();
			def += weapon.getDef();
		}
		
		if(specialWeapon != null){
			maxHp += specialWeapon.getMaxHP();
			maxMp += specialWeapon.getMaxMP();
			def += specialWeapon.getDef();
		}
		
		if(armor != null){
			maxHp += armor.getMaxHP();
			maxMp += armor.getMaxMP();
			def += armor.getDef();
		}
		
		for(StatusEffect s:statusEffects){
			switch(s.getType()){
				case TYPE_HP_BUFF:		maxHp += s.getLevel();	break;
				case TYPE_HP_DEBUFF:	maxHp -= s.getLevel();	break;
				case TYPE_MP_BUFF:		maxMp += s.getLevel();	break;
				case TYPE_MP_DEBUFF:	maxMp -= s.getLevel();	break;
				case TYPE_DEF_BUFF:		def += s.getLevel();	break;
				case TYPE_DEF_DEBUFF:	def -= s.getLevel();	break;
			}
		}
	}
	
	public void restoreStats(){
		hp = maxHp;
		mp = maxMp;
	}
	
	public int getClassType(){
		return classType;
	}
	
	public String getClassName(){
		return getClassName(classType);
	}
	
	public static String getClassName(int classType){
		switch(classType){
			case CLASS_KNIGHT:	return "Knight";
			case CLASS_ARCHER:	return "Archer";
			case CLASS_MAGE:	return "Mage";
		}
		return "";
	}
	
	public void setHP(int hp){
		this.hp = hp;
		
		if(this.hp > maxHp)
			this.hp = maxHp;
	}
	
	public void addHP(int hp){
		this.hp += hp;
		
		if(this.hp > maxHp)
			this.hp = maxHp;
	}
	
	public int getHP(){
		return hp;
	}
	
	public int getMaxHP(){
		return maxHp;
	}
	
	public int getBaseMaxHP(){
		return baseMaxHp;
	}
	
	public void setMP(int mp){
		this.mp = mp;
		
		if(this.mp > maxMp)
			this.mp = maxMp;
	}
	
	public void addMP(int mp){
		this.mp += mp;
		
		if(this.mp > maxMp)
			this.mp = maxMp;
	}
	
	public int getMP(){
		return mp;
	}
	
	public int getMaxMP(){
		return maxMp;
	}
	
	public int getBaseMaxMP(){
		return baseMaxMp;
	}
	
	public int getDef(){
		return def;
	}
	
	public int getBaseDef(){
		return baseDef;
	}
	
	public int getLevel(){
		return level;
	}
	
	public void setXP(int xp){
		this.xp = xp;
		
		if(xp >= xpReq)
			levelUp();
	}

	public void addXP(int xp){
		this.xp += xp;
		
		if(xp >= xpReq)
			levelUp();
	}
	
	public int getXP(){
		return xp;
	}
	
	public int getXPReq(){
		return xpReq;
	}
	
	public void addGold(int gold){
		this.gold += gold;
	}
	
	public int getGold(){
		return gold;
	}
	
	public void addStatusEffect(StatusEffect statusEffect){
		
		for(int i = 0; i < statusEffects.size(); i++){
			StatusEffect s = statusEffects.get(i);
			
			if(s.getType() == statusEffect.getType()){
				s = new StatusEffect(
					s.getType(),
					Math.max(s.getLevel(), statusEffect.getLevel()),
					Math.max(s.getDuration(), statusEffect.getDuration()),
					0, false
				);
				
				statusEffects.set(i, s);
				return;
			}
		}
		
		statusEffects.add(statusEffect);
	}
	
	public ArrayList<StatusEffect> getStatusEffects(){
		return statusEffects;
	}
	
	public Weapon getWeapon(){
		return weapon;
	}
	
	public Weapon getSpecialWeapon(){
		return specialWeapon;
	}
	
	public EquippableItem getArmor(){
		return armor;
	}
	
	public void equip(EquippableItem item){
		
		if(item instanceof Weapon){
			Weapon w = (Weapon)item;
			
			if(!w.isSpecialWeapon())
				weapon = w;
			else
				specialWeapon = w;
		}
		
		if(!(item instanceof Weapon))
			armor = item;
		
		updateStats();
		updateSprite();
	}
	
	public Item unequip(int slot){
		
		Item item = null;
		
		switch(slot){
			case 0:
				item = weapon;
				weapon = null;
				break;
			
			case 1:
				item = specialWeapon;
				specialWeapon = null;
				break;
			
			case 2:
				item = armor;
				armor = null;
				break;
		}

		updateStats();
		updateSprite();
		
		return item;
	}
	
	public void useItem(ConsumableItem item){
		
		addHP(item.getHP());
		addMP(item.getMP());
		
		StatusEffect[] se = item.getStatusEffects();
		
		if(se != null){
			for(StatusEffect s:se)
				addStatusEffect(s);
			
			updateStats();
		}
	}
	
	public int getItemCount(Item item){
		
		int n = 0;
		
		for(int i = 0; i < inventory.size(); i++)
			if(inventory.get(i).equals(item))
				n += inventoryCounts.get(i);
		
		return n;
	}
	
	public void addItem(Item item){
		addItem(item, 1);
	}
	
	public void addItem(Item item, int count){
		
		if(item == null)
			return;
		
		for(int i = 0; i < inventory.size(); i++){
			
			// Add to stack
			if(inventory.get(i).equals(item)){
				int itemCount = inventoryCounts.get(i);
				int added = Math.min(item.getStackSize() - itemCount, count);
				
				if(added == 0)
					continue;
				
				itemCount += added;
				inventoryCounts.set(i, itemCount);
				
				count -= added;
				
				if(count == 0)
					return;
			}
		}
		
		while(count > 0){
			int added = Math.min(item.getStackSize(), count);
			
			inventory.add(item);
			inventoryCounts.add(added);
			
			count -= added;
		}
	}
	
	public void removeItem(Item item){
		removeItem(item, 1);
	}
	
	public void removeItem(Item item, int count){
		
		if(item == null)
			return;
		
		for(int i = inventory.size() - 1; i >= 0; i--){
			if(inventory.get(i).equals(item)){
				
				int itemCount = inventoryCounts.get(i);
				int dropped = Math.min(itemCount, count);
				
				itemCount -= dropped;
				inventoryCounts.set(i, itemCount);
				
				if(itemCount == 0){
					inventory.remove(i);
					inventoryCounts.remove(i);
				}
				
				count -= dropped;
				
				if(count == 0)
					return;
			}
		}
	}
	
	public boolean isInventoryFull(){
		return inventory.size() == inventorySize;
	}
	
	public ArrayList<Item> getInventory(){
		return inventory;
	}
	
	public ArrayList<Integer> getInventoryCounts(){
		return inventoryCounts;
	}
	
	public int getInventorySize(){
		return inventorySize;
	}
	
	
	
	
	
	// Map screen
	
	public void update(){
		
		// Movement
		if(moveTime > 0){
			switch(direction){
				case 0: x += 1f/MOVE_TIME;	break;
				case 1: x -= 1f/MOVE_TIME;	break;
				case 2: y += 1f/MOVE_TIME;	break;
				case 3: y -= 1f/MOVE_TIME;	break;
			}
			
			if(moveTime % (MOVE_TIME/3) == 0)
				spriteCycle++;
			
			moveTime--;
		}
		
		if(moveTime == 0){
			spriteCycle = 0;
			
			if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_RIGHT)){
				direction = DIR_RIGHT;
				
				if(movementsAllowed[0]){
					tileX++;
					moveTime = MOVE_TIME;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_LEFT)){
				direction = DIR_LEFT;
				
				if(movementsAllowed[1]){
					tileX--;
					moveTime = MOVE_TIME;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_DOWN)){
				direction = DIR_DOWN;
				
				if(movementsAllowed[2]){
					tileY++;
					moveTime = MOVE_TIME;
				}
			}
			else if(KeyboardListener.isKeyDown(GLFW.GLFW_KEY_UP)){
				direction = DIR_UP;
				
				if(movementsAllowed[3]){
					tileY--;
					moveTime = MOVE_TIME;
				}
			}
		}
		
		updateSprite();
	}
	
	private void updateSprite(){
		
		int x = 0;
		int y = armor == null ? 0 : 64 + classType*64;
		
		if(spriteCycle == 0 || spriteCycle == 2)
			x = 0;
		if(spriteCycle == 1)
			x = 32;
		if(spriteCycle == 3)
			x = 64;
		
		if(direction == DIR_LEFT || direction == DIR_UP)
			y += 32;

		if(direction == DIR_RIGHT || direction == DIR_LEFT)
			sprite.setScaleX(1);
		else
			sprite.setScaleX(-1);
		
		sprite.setX(x);
		sprite.setY(y);
		
		// Weapons
		x += 96;
		
		if(direction == DIR_DOWN || direction == DIR_UP){
			x += 96;
		}
		
		y = 64 + classType*64;
		if(direction == DIR_LEFT || direction == DIR_UP)
			y += 32;
		
		weaponSprite.setX(x);
		weaponSprite.setY(y);
		specialWeaponSprite.setX(x + 96);
		specialWeaponSprite.setY(y);
		
		sprite.genTextureCoords();
		weaponSprite.genTextureCoords();
		specialWeaponSprite.genTextureCoords();
	}
	
	public float[] getWeaponTexCoords(){
		return weaponSprite.getTextureCoords();
	}
	
	public float[] getSpecialWeaponTexCoords(){
		return specialWeaponSprite.getTextureCoords();
	}
	
	public int getTileX(){
		return tileX;
	}
	
	public int getTileY(){
		return tileY;
	}
	
	public int getTileZ(){
		return tileZ;
	}
	
	public int getDirection(){
		return direction;
	}
	
	public void setMovementsAllowed(boolean[] movementsAllowed){
		this.movementsAllowed = movementsAllowed;
	}
}
