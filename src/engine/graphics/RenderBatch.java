package engine.graphics;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;

import engine.Color;
import engine.objects.GameEntity;
import engine.objects.GameObject;
import engine.objects.Player;
import engine.objects.items.EquippableItem;
import engine.objects.items.Weapon;
import engine.ui.Icon;
import engine.ui.TextChar;
import engine.ui.UISpriteElem;

/*
 * 		RenderBatch.java
 * 		
 * 		Purpose:	Stores VAOs/VBOs for a set of entities
 * 		Notes:		
 * 		
 */

public class RenderBatch{
	
	public static final int
		UPDATE_POS = 0b000001,
		UPDATE_SZE = 0b000010,
		UPDATE_TEX = 0b000100,
		UPDATE_RTS = 0b001000,
		UPDATE_COL = 0b010000,
		UPDATE_ALP = 0b100000,
		
		UPDATE_NONE				= 0,
		UPDATE_ALL				= 0b111111,
		UPDATE_BASIC			= 0b000111,
		UPDATE_WITH_ROTATION	= 0b001111,
		UPDATE_WITH_COLOR		= 0b010111;
	
	
	private final int shader;
	
	// Number of quads to render
	private int size;
	
	private final int capacity;
	
	private FloatBuffer posBuffer, texBuffer, rtsBuffer, colBuffer1, colBuffer2, colBuffer3, alpBuffer;
	private ShortBuffer szeBuffer;
	
	private final int vao, pos, sze, tex, rts, col1, col2, col3, alp;
	
	private final int textureID;
	
	// Which buffers to update
	private int updates;
	private boolean uPOS, uSZE, uTEX, uCOL, uRTS, uALP;
	
	// If true, positions are adjusted to be isometric
	private boolean mapScreenMode;
	
	public RenderBatch(int shader, int capacity, int textureID, int updates, boolean mapScreenMode){
		this.shader = shader;
		this.capacity = capacity;
		this.textureID = textureID;
		this.updates = updates;
		this.mapScreenMode = mapScreenMode;
		
		vao = glGenVertexArrays();
		pos = glGenBuffers();
		sze = glGenBuffers();
		tex = glGenBuffers();
		rts = glGenBuffers();
		col1 = shader == 1 ? glGenBuffers() : 0;
		col2 = shader == 1 ? glGenBuffers() : 0;
		col3 = shader == 1 ? glGenBuffers() : 0;
		//alp = glGenBuffers();
		alp = 0;
		
		init();
	}
	
	private void init(){
		posBuffer = BufferUtils.createFloatBuffer(capacity*2);
		szeBuffer = BufferUtils.createShortBuffer(capacity*2);
		texBuffer = BufferUtils.createFloatBuffer(capacity*4);
		rtsBuffer = BufferUtils.createFloatBuffer(capacity*3);
		//alpBuffer = BufferUtils.createFloatBuffer(capacity);
		
		if(shader == 1){
			colBuffer1 = BufferUtils.createFloatBuffer(capacity*3);
			colBuffer2 = BufferUtils.createFloatBuffer(capacity*3);
			colBuffer3 = BufferUtils.createFloatBuffer(capacity*3);
		}
		
		uPOS = true;
		uSZE = true;
		uTEX = true;
		uRTS = true;
		uCOL = shader == 1;
		//uALP = true;
		
		return;
	}
	
	public int getShader(){
		return shader;
	}
	
	public int getTextureID(){
		return textureID;
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	public void render(){
		enable();
		glDrawArrays(GL_POINTS, 0, size);
		disable();
	}
	
	public void bindTexture(){
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D, textureID);
	}
	
	private void enable(){
		
		// Bind VAO
		glBindVertexArray(vao);
		
		// Enable arrays
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		if(uRTS) glEnableVertexAttribArray(3);
		if(shader == 1){
			glEnableVertexAttribArray(4);
			glEnableVertexAttribArray(5);
			glEnableVertexAttribArray(6);
		}
		//glEnableVertexAttribArray(5);
		
		// Set blend mode
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	
	private void disable(){
		// Disable blending
		glDisable(GL_BLEND);
		
		// Disable arrays
		//glDisableVertexAttribArray(5);
		if(shader == 1){
			glDisableVertexAttribArray(6);
			glDisableVertexAttribArray(5);
			glDisableVertexAttribArray(4);
		}
		if(uRTS) glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		
		// Unbind VAO
		glBindVertexArray(0);
	}
	
	public void cleanup(){
		glDeleteVertexArrays(vao);
		glDeleteBuffers(pos);
		glDeleteBuffers(sze);
		glDeleteBuffers(tex);
		glDeleteBuffers(rts);
		glDeleteBuffers(col1);
		glDeleteBuffers(col2);
		glDeleteBuffers(col3);
		glDeleteBuffers(alp);
	}
	
	public void updateWithObjects(GameObject[] objects, float[] positions, int camX, int camY){

		ArrayList<GameObject> ol = new ArrayList<GameObject>();
		
		// Take only visible entities
		for(GameObject o:objects)
			if(o.isVisible())
				ol.add(o);
		
		size = ol.size();
		
		if(ol.isEmpty())
			return;
		
		// VAO arguments
		float[]
			texCoords	= null,
			rotations	= null,
			alphas		= null;
		
		short[] sizes = null;
		
		if(uSZE) sizes		= new short[size*2];
		if(uTEX) texCoords	= new float[size*4];
		if(uRTS) rotations	= new float[size];
		if(uALP) alphas		= new float[size];
		
		float[] positions2 = new float[size*2];
		
		for(int i = 0; i < ol.size(); i++){
			
			GameObject o = ol.get(i);
			
			// Get animated sprite
			Sprite s = o.getSprite();
			
			// Tex coords
			float[] t = s.getTextureCoords();
			
			// Rotation
			float r = s.getRotation();
			
			// Fill arrays
			if(uPOS && mapScreenMode){
				float x = positions[i*3];
				float y = positions[i*3 + 1];
				float z = positions[i*3 + 2];
				
				positions2[i*2]		= camX + (x - y)*16;
				positions2[i*2 + 1]	= camY + (x + y - z)*8;
			}
			
			if(uSZE){
				sizes[i*2]		= (short)s.getScaledWidth();
				sizes[i*2 + 1]	= (short)s.getScaledHeight();
			}
			
			if(uTEX){
				for(int j = 0; j < 4; j++)
					texCoords[i*4 + j] = t[j];
			}
			
			if(uRTS)
				rotations[i] = (float)Math.toRadians(r);
			
			if(uALP)
				alphas[i] = s.getAlpha();
		}
		
		updateVBOs(positions2, sizes, texCoords, null, rotations, alphas);
	}
	
	public void updatePlayer(Player player, int camX, int camY){
		
		if(!player.isVisible())
			return;
		
		EquippableItem armor = player.getArmor();
		Weapon weapon = player.getWeapon();
		Weapon specialWeapon = player.getSpecialWeapon();
		
		// Player, weapon, special
		
		size = 1 +
			(weapon			== null ? 0 : 1) +
			(specialWeapon	== null ? 0 : 1);
		
		float[]
			positions	= new float[size*2],
			texCoords	= new float[size*4],
			rotations	= new float[size],
			alphas		= new float[size];
		
		float[][] colors = new float[3][size*3];
		
		short[] sizes = new short[size*2];
		
		
		float x = player.getX();
		float y = player.getY();
		
		for(int i = 0; i < size; i++){
			if(!mapScreenMode){
				positions[i*2]		= camX + (int)x;
				positions[i*2 + 1]	= camY + (int)y;
			}
			else{
				positions[i*2]		= camX + (int)((x - y)*16);
				positions[i*2 + 1]	= camY + (int)((x + y)*8);
			}
		}
		
		
		Sprite sprite = player.getSprite();
		
		short[] s = {(short)sprite.getScaledWidth(), (short)sprite.getScaledHeight()};
		
		for(int i = 0; i < size; i++){
			if(i == 1){
				s[0] = (short)Math.abs(s[0]);
				s[1] = (short)Math.abs(s[1]);
			}
			
			sizes[i*2]		= s[0];
			sizes[i*2 + 1]	= s[1];
		}
		
		
		float[] t = player.getSprite().getTextureCoords();
		Color[] c = null;
		c = armor == null ? new Color[]{Color.BLACK} : armor.getPlayerColors();
		
		for(int i = 0; i < t.length; i++)
			texCoords[i] = t[i];
		
		for(int j = 0; j < c.length; j++){
			colors[j][0]	= c[j].getRed()/255f;
			colors[j][1]	= c[j].getGreen()/255f;
			colors[j][2]	= c[j].getBlue()/255f;
		}
		
		// Weapon
		int offset = 1;
		if(weapon != null){
			t = player.getWeaponTexCoords();
			c = weapon.getColors();
			
			for(int i = 0; i < t.length; i++)
				texCoords[i + offset*4] = t[i];
			
			for(int j = 0; j < c.length; j++){
				colors[j][offset*3]		= c[j].getRed()/255f;
				colors[j][offset*3 + 1]	= c[j].getGreen()/255f;
				colors[j][offset*3 + 2]	= c[j].getBlue()/255f;
			}
			
			offset++;
		}
		
		// Special weapon
		if(specialWeapon != null){
			t = player.getSpecialWeaponTexCoords();
			c = specialWeapon.getColors();
			
			for(int i = 0; i < t.length; i++)
				texCoords[i + offset*4] = t[i];
			
			for(int j = 0; j < c.length; j++){
				colors[j][offset*3]		= c[j].getRed()/255f;
				colors[j][offset*3 + 1]	= c[j].getGreen()/255f;
				colors[j][offset*3 + 2]	= c[j].getBlue()/255f;
			}
		}
		
		updateVBOs(positions, sizes, texCoords, colors, rotations, alphas);
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void updateWithEntities(ArrayList entityList, int camX, int camY){
		
		ArrayList<GameEntity> el = new ArrayList<GameEntity>();
		
		// Take only visible entities
		for(GameEntity e:(ArrayList<GameEntity>)entityList)
			if(e.isVisible())
				el.add(e);
		
		size = el.size();
		
		if(el.isEmpty())
			return;
		
		// VAO arguments
		float[]
			positions	= null,
			texCoords	= null,
			rotations	= null,
			alphas		= null;
		
		float[][] colors = null;
		
		short[] sizes = null;
		
		if(uPOS) positions	= new float[size*2];
		if(uSZE) sizes		= new short[size*2];
		if(uTEX) texCoords	= new float[size*4];
		if(uRTS) rotations	= new float[size];
		if(uCOL) colors		= new float[3][size*3];
		if(uALP) alphas		= new float[size];
		
		for(int i = 0; i < el.size(); i++){
			
			GameEntity e = el.get(i);
			
			// Get animated sprite
			Sprite s = e.getSprite();
			
			// Fill arrays
			if(uPOS){
				float x = e.getX();
				float y = e.getY();
				
				if(!mapScreenMode){
					positions[i*2]		= camX + (int)x;
					positions[i*2 + 1]	= camY + (int)y;
				}
				else{
					positions[i*2]		= camX + (int)((x - y)*16);
					positions[i*2 + 1]	= camY + (int)((x + y)*8);
				}
			}
			
			if(uSZE){
				sizes[i*2]		= (short)s.getScaledWidth();
				sizes[i*2 + 1]	= (short)s.getScaledHeight();
			}
			
			if(uTEX){
				float[] t = s.getTextureCoords();
				
				for(int j = 0; j < 4; j++)
					texCoords[i*4 + j] = t[j];
			}
			
			if(uRTS)
				rotations[i] = (float)Math.toRadians(s.getRotation());
			
			if(uALP)
				alphas[i] = s.getAlpha();
		}
		
		updateVBOs(positions, sizes, texCoords, colors, rotations, alphas);
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	public void updateWithUIElements(ArrayList elementList, int camX, int camY){
		
		ArrayList<UISpriteElem> el = new ArrayList<UISpriteElem>();
		
		// Take only visible entities
		for(UISpriteElem e:(ArrayList<UISpriteElem>)elementList)
			if(e.isVisible())
				el.add(e);
		
		size = el.size();
		
		if(el.isEmpty())
			return;
		
		// VAO arguments
		float[]
			positions	= null,
			texCoords	= null,
			rotations	= null,
			alphas		= null;

		float[][] colors = null;
		
		short[]
			sizes = null;
		
		if(uPOS) positions	= new float[size*2];
		if(uSZE) sizes		= new short[size*2];
		if(uTEX) texCoords	= new float[size*4];
		if(uRTS) rotations	= new float[size];
		if(uCOL) colors		= new float[3][size*3];
		if(uALP) alphas		= new float[size];
		
		for(int i = 0; i < el.size(); i++){
			
			UISpriteElem e = el.get(i);
			
			// Get animated sprite
			Sprite s = e.getSprite();
			
			// Fill arrays
			if(uPOS){
				positions[i*2]		= camX + e.getX();
				positions[i*2 + 1]	= camY + e.getY();
			}
			
			if(uSZE){
				sizes[i*2]		= (short)s.getScaledWidth();
				sizes[i*2 + 1]	= (short)s.getScaledHeight();
			}
			
			if(uTEX){
				float[] t = s.getTextureCoords();
				
				for(int j = 0; j < 4; j++)
					texCoords[i*4 + j] = t[j];
			}
			
			if(uCOL){
				float[][] c = null;
				
				if(e instanceof TextChar)
					c = new float[][]{((TextChar)e).getColor(), {1, 0, 0}};
				
				if(e instanceof Icon)
					c = ((Icon)e).getColors();
				
				for(int j = 0; j < c.length; j++){
					colors[j][i*3]		= c[j][0];
					colors[j][i*3 + 1]	= c[j][1];
					colors[j][i*3 + 2]	= c[j][2];
				}
			}
			
			if(uRTS)
				rotations[i] = (float)Math.toRadians(s.getRotation());
			
			if(uALP)
				alphas[i] = s.getAlpha();
		}
		
		updateVBOs(positions, sizes, texCoords, colors, rotations, alphas);
	}
	
	private void updateVBOs(float[] positions, short[] sizes, float[] texCoords, float[][] colors, float[] rotations, float[] alphas){
		
		glBindVertexArray(vao);
		
		// Vertices
		if(uPOS){
			posBuffer.clear();
			posBuffer.put(positions);
			posBuffer.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, pos);
			glBufferData(GL_ARRAY_BUFFER, posBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
			
			if((updates & UPDATE_POS) == 0)
				uPOS = false;
		}
		
		// Sizes
		if(uSZE){
			szeBuffer.clear();
			szeBuffer.put(sizes);
			szeBuffer.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, sze);
			glBufferData(GL_ARRAY_BUFFER, szeBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(1, 2, GL_SHORT, false, 0, 0);
			
			if((updates & UPDATE_SZE) == 0)
				uSZE = false;
		}
		
		// Texture coords
		if(uTEX){
			texBuffer.clear();
			texBuffer.put(texCoords);
			texBuffer.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, tex);
			glBufferData(GL_ARRAY_BUFFER, texBuffer, GL_STATIC_DRAW);
			glVertexAttribPointer(2, 4, GL_FLOAT, false, 0, 0);
			
			if((updates & UPDATE_TEX) == 0)
				uTEX = false;
		}
		
		// Rotation
		if(uRTS){
			rtsBuffer.clear();
			rtsBuffer.put(rotations);
			rtsBuffer.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, rts);
			glBufferData(GL_ARRAY_BUFFER, rtsBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(3, 1, GL_FLOAT, false, 0, 0);
			
			if((updates & UPDATE_RTS) == 0)
				uRTS = false;
		}
		
		// Colors
		if(uCOL){
			colBuffer1.clear();
			colBuffer1.put(colors[0]);
			colBuffer1.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, col1);
			glBufferData(GL_ARRAY_BUFFER, colBuffer1, GL_STATIC_DRAW);
			glVertexAttribPointer(4, 3, GL_FLOAT, false, 0, 0);
			
			colBuffer2.clear();
			colBuffer2.put(colors[1]);
			colBuffer2.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, col2);
			glBufferData(GL_ARRAY_BUFFER, colBuffer2, GL_STATIC_DRAW);
			glVertexAttribPointer(5, 3, GL_FLOAT, false, 0, 0);
			
			colBuffer3.clear();
			colBuffer3.put(colors[2]);
			colBuffer3.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, col3);
			glBufferData(GL_ARRAY_BUFFER, colBuffer3, GL_STATIC_DRAW);
			glVertexAttribPointer(6, 3, GL_FLOAT, false, 0, 0);
			
			if((updates & UPDATE_COL) == 0)
				uCOL = false;
		}
		
		// Alpha
		if(uALP){
			alpBuffer.clear();
			alpBuffer.put(alphas);
			alpBuffer.flip();
			
			glBindBuffer(GL_ARRAY_BUFFER, alp);
			glBufferData(GL_ARRAY_BUFFER, alpBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(6, 1, GL_FLOAT, false, 0, 0);
			
			if((updates & UPDATE_ALP) == 0)
				uALP = false;
		}
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glBindVertexArray(0);
	}
}
