package engine.graphics;

import static engine.graphics.RenderBatch.*;

import java.util.ArrayList;

import engine.objects.Map;
import engine.objects.Player;
import engine.objects.Tile;
import engine.ui.Bar;
import engine.ui.Icon;
import engine.ui.Text;
import engine.ui.TextArea;
import engine.ui.TextAreaPart;
import engine.ui.TextChar;
import engine.ui.menus.Menu;

/*
 * 		Renderer.java
 * 		
 * 		Purpose:	Renders game objects.
 * 		Notes:		
 * 		
 */

public class Renderer{
	
	private ShaderProgram basicShader, colorShader;
	
	private ArrayList<RenderBatch> renderBatches;
	
	
	private RenderBatch
		rbPlayer,
		rbNPCs,
		rbTiles,
		rbTextAreas,
		rbText,
		rbIcons;
	
	
	private boolean firstRender;
	
	private TextureCache tc;
	
	// Map screen camera position
	private int camX, camY;
	
	public Renderer(TextureCache tc){
		this.tc = tc;
	}
	
	public void init(){
		
		// Init shaders
		basicShader = new ShaderProgram("quad", "quad", "basic");
		basicShader.bindAttrib(0, "position");
		basicShader.bindAttrib(1, "size");
		basicShader.bindAttrib(2, "texCoords");
		basicShader.bindAttrib(3, "rotation");
		basicShader.link();
		
		colorShader = new ShaderProgram("colorQuad", "colorQuad", "basic");
		colorShader.bindAttrib(0, "position");
		colorShader.bindAttrib(1, "size");
		colorShader.bindAttrib(2, "texCoords");
		colorShader.bindAttrib(3, "rotation");
		colorShader.bindAttrib(4, "color1");
		colorShader.bindAttrib(5, "color2");
		colorShader.bindAttrib(6, "color3");
		colorShader.link();
		
		renderBatches = new ArrayList<RenderBatch>();
	}
	
	// Initialize rendering for MapScreen
	public void initMainScreen(){
		firstRender = true;
		
		rbPlayer	= new RenderBatch(1, 4, tc.cache("player.png").getID(), UPDATE_WITH_COLOR, true);
		rbNPCs		= new RenderBatch(1, 4, tc.cache("player.png").getID(), UPDATE_WITH_COLOR, true);
		rbTiles		= new RenderBatch(0, 512, tc.cache("tiles/01.png").getID(), UPDATE_BASIC, true);
		rbTextAreas	= new RenderBatch(0, 64, tc.cache("ui/text_area.png").getID(), UPDATE_WITH_ROTATION, false);
		rbText		= new RenderBatch(1, 512, tc.cache("ui/fonts/01.png").getID(), UPDATE_WITH_COLOR, false);
		rbIcons		= new RenderBatch(1, 64, tc.cache("ui/icons.png").getID(), UPDATE_WITH_COLOR, false);
		
		// Add in order of rendering
		renderBatches.add(rbTiles);
		renderBatches.add(rbPlayer);
		renderBatches.add(rbNPCs);
		renderBatches.add(rbTextAreas);
		renderBatches.add(rbIcons);
		renderBatches.add(rbText);
	}
	
	public void setCamPos(int camX, int camY){
		this.camX = camX;
		this.camY = camY;
	}
	
	public void updateTiles(Map map){
		
		Tile[][][] tiles = map.getTiles();
		
		int n = 0;
		
		// Count tiles
		for(Tile[][] ta:tiles)
			for(Tile[] ta2:ta)
				for(Tile t:ta2)
					if(t.isVisible())
						n++;
		
		Tile[] tiles2 = new Tile[n];
		float[] positions = new float[n*3];
		
		n = 0;
		
		// Set positions
		for(int x = 0; x < tiles.length; x++){
			for(int y = 0; y < tiles[x].length; y++){
				for(int z = 0; z < tiles[x][y].length; z++){
					Tile t = tiles[x][y][z];
					
					if(!t.isVisible())
						continue;
					
					// Set position
					positions[n*3]		= x;
					positions[n*3 + 1]	= y;
					positions[n*3 + 2]	= z;
					
					// Add tile
					tiles2[n] = t;
					n++;
				}
			}
		}
		
		rbTiles.updateWithObjects(tiles2, positions, camX, camY + 20);
	}
	
	public void updatePlayer(Player player){
		rbPlayer.updatePlayer(player, camX, camY);
	}
	
	public void updateMenus(ArrayList<Menu> menus){
		ArrayList<TextAreaPart> textAreaParts = new ArrayList<TextAreaPart>();
		ArrayList<Icon> icons = new ArrayList<Icon>();
		ArrayList<TextChar> textChars = new ArrayList<TextChar>();
		
		for(Menu m:menus){
			if(m.isOpen()){
				for(TextArea ta:m.getTextAreas())
					if(ta.isVisible())
						for(TextAreaPart t:ta.getParts())
							if(t.isVisible())
								textAreaParts.add(t);
				
				for(Bar b:m.getBars())
					if(b.isVisible())
						for(Icon i:b.getParts())
							if(i.isVisible())
								icons.add(i);
				
				for(Icon i:m.getIcons())
					if(i.isVisible())
						icons.add(i);
				
				for(Text t:m.getTexts())
					if(t.isVisible())
						for(TextChar c:t.getChars())
							if(c.isVisible())
								textChars.add(c);
			}
		}
		
		rbTextAreas.updateWithUIElements(textAreaParts, 0, 0);
		rbIcons.updateWithUIElements(icons, 0, 0);
		rbText.updateWithUIElements(textChars, 0, 0);
	}
	
	public void render(){
		for(int i = 0; i < renderBatches.size(); i++){
			
			RenderBatch rb = renderBatches.get(i);
			
			if(rb.isEmpty() && !firstRender)
				continue;
			
			if(rb.getShader() == 0)
				basicShader.use();
			else if(rb.getShader() == 1)
				colorShader.use();
			
			// Don't bind texture again if last batch had same texture
			if(rb.getTextureID() != -1 && (i == 0 || rb.getTextureID() != renderBatches.get(i - 1).getTextureID()))
				rb.bindTexture();
			
			rb.render();
		}
		
		firstRender = false;
	}
	
	public void cleanup(){
		basicShader.destroy();
		
		for(RenderBatch rb:renderBatches)
			rb.cleanup();
		
		renderBatches.clear();
	}
}
