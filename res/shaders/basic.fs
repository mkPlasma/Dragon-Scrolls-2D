#version 150

uniform sampler2D tex;

in vec2 fTexCoords;
in vec3 fColor1;
in vec3 fColor2;
in vec3 fColor3;

out vec4 fragColor;

void main(){
    fragColor = texture(tex, fTexCoords);
    
    // Coloring
    vec3 b = vec3(0.0, 0.0, 0.0);
    if(fColor1 != b || fColor2 != b || fColor3 != b){
        if(fragColor.r > 0){
            fragColor.gb = vec2(fragColor.r, fragColor.r);
            fragColor.rgb *= fColor1;
        }
        else if(fragColor.g > 0){
            fragColor.rb = vec2(fragColor.g, fragColor.g);
            fragColor.rgb *= fColor2 == b ? fColor1 : fColor2;
        }
        else if(fragColor.b > 0){
            fragColor.rg = vec2(fragColor.b, fragColor.b);
            fragColor.rgb *= fColor3 == b ? fColor1 : fColor3;
        }
    }
}
