#version 150

in vec2 position;
in vec2 size;
in vec4 texCoords;
in float rotation;
in vec3 color1;
in vec3 color2;
in vec3 color3;

out vec2 gSize;
out vec4 gTexCoords;
out float gRotation;
out vec3 gColor1;
out vec3 gColor2;
out vec3 gColor3;


void main(){
    gSize = size;
    gTexCoords = texCoords;
    gRotation = rotation;
    gColor1 = color1;
    gColor2 = color2;
    gColor3 = color3;
    
    gl_Position = vec4(position, 0.0, 1.0);
}