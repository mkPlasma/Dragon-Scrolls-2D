#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec2 gSize[];
in vec4 gTexCoords[];
in float gRotation[];
in vec3 gColor1[];
in vec3 gColor2[];
in vec3 gColor3[];

out vec2 fTexCoords;
out vec3 fColor1;
out vec3 fColor2;
out vec3 fColor3;

void vertex(vec2 pos2, float tx, float ty){
    
    vec4 pos = gl_in[0].gl_Position;
    
    // Rotate
    float r = gRotation[0];
    pos2 *= mat2(cos(r), -sin(r), sin(r), cos(r));
    
    pos += vec4(pos2, 0.0, 0.0);
    
    // Normalize
    pos.x = pos.x/320.0 - 1.0;
    pos.y = -(pos.y/240.0 - 1.0);
    
    fTexCoords = vec2(tx, ty);
    gl_Position = pos;
    EmitVertex();
}

void main(){
    
    vec4 tx = gTexCoords[0];
    
    float x = gSize[0].x/2;
    float y = gSize[0].y/2;
    
    fColor1 = gColor1[0];
    fColor2 = gColor2[0];
    fColor3 = gColor3[0];
    
    vertex(vec2(-x, -y), tx.x, tx.z);
    vertex(vec2(x, -y), tx.y, tx.z);
    vertex(vec2(-x, y), tx.x, tx.w);
    vertex(vec2(x, y), tx.y, tx.w);
    
    EndPrimitive();
}